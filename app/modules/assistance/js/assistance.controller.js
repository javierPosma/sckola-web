(function(){
  'use strict';

  angular
  .module('sckola.assistance')
  .controller('AssistanceCTRL', AssistanceCTRL);

  AssistanceCTRL.$inject = ['authentication', '$scope', '$rootScope', '$uibModal', '$state', 'StudentsBySection', 'ComunidadMaterias', 'AssistanceStudents', 'AssistanceStudentsEdit', 'ComunidadMateriasByType', '$timeout'];
  function AssistanceCTRL(authentication, $scope, $rootScope, $uibModal, $state, StudentsBySection, ComunidadMaterias, AssistanceStudents, AssistanceStudentsEdit, ComunidadMateriasByType, $timeout) {
    var vm = this;
    vm.message = "cargando pagina";
    vm.messageInitial = "Por favor seleccione una comunidad";
    vm.messageHelp = "";
    vm.assistanceSavedSuccess = "¡Felicitaciones! La asistencia se guardo satisfactoriamente";
    vm.clase = [];
    vm.show = false;
    vm.showError = false;
    vm.errorMessage = "¡Oops! Algo ha salido mal";
    vm.showHelp = false;
    vm.assistencia = {"date": null, "matterCommunitySectionId": null, "userAssistList": []};
    vm.date = new Date();
    vm.dateActual = new Date();
    vm.edit = false;
    vm.showSuccessAssistance = false;
    vm.showErrorAssistance = false;

    vm.user = authentication.getUser();
    vm.communitySelected = authentication.getCommunity();
    var permission = authentication.currentUser();

    $rootScope.tokenLoging = authentication.isLoggedIn();

    if(vm.communitySelected==null){
      vm.showHelp = true;
      vm.helpMessage = "Debes seleccionar una comunidad. ¡Para seleccionar una comunidad haga click aqui!";
    }

    /**
    * @name showErrorMessage
    * @desc Shows error message to the user
    * @memberOf Controllers.AssistanceController
    */
    vm.showErrorMessage = function(){
      vm.showError = true;
      $timeout(function(){
        vm.showError = false;
      },2500);
    };


    /**
    * @name helpUser
    * @desc Depending of the user state, if it has a community or not, a mmater or not,
    *       the state of the view is changed to the requirement.
    * @memberOf Controllers.AssistanceController
    */
    vm.helpUser = function(){
      if(vm.communitySelected == null){
        $state.go("communities");
      }
      else if(vm.clases.length < 1){
        $state.go("matterHome")
      }
      else if(vm.respaldoEstudiantes.length < 1){
        $state.go("students");
      }
    };

    vm.loadInitial = function () {
      vm.showMessageInitial = false;
      if (vm.communitySelected !== undefined && vm.communitySelected.id !== undefined) {
        ComunidadMaterias.get({communityId: vm.communitySelected.communityOrigin.id, roleId: 1, userId: vm.user.id},
          function (success) {
            vm.showMessageInitial = false;
            vm.messageClaseShow = false;
            vm.typeIntegral = false;
            vm.typeNotIntegral = false;
            vm.showClaseUnic =false;
            vm.show = false;
            vm.showType=false;
            vm.showClases = false;
            vm.seleccion=false;
            vm.showStudentsMessage=false;
            vm.clases = success.response;
            if(vm.clases.length > 1){
              vm.showClases = true;
              vm.showType=true;
              vm.showClaseUnic = false;
              vm.clase=[];
              for (var i = 0; i < vm.clases.length; i++) {
                if(vm.clases[i].type == "INTEGRAL"){
                  vm.typeIntegral = true;
                }else{
                  vm.typeNotIntegral = true;
                }
                vm.materSeccion = {
                  "id": vm.clases[i].id,
                  "name": vm.clases[i].matterCommunity.name + " " + vm.clases[i].section.name,
                  "sectionId": vm.clases[i].section.id
                };
                vm.clase.push(vm.materSeccion)
              }
              if(!vm.typeNotIntegral && vm.typeIntegral){
                vm.typeClass(true);
                vm.showType=false;
              }
              if(!vm.typeIntegral && vm.typeNotIntegral){
                vm.typeClass(false);
                vm.showType=false;
              }
              $timeout(function(){
                vm.terminate = true;
              }, 1000);
            }else{
              if(vm.clases[0] != null){
                vm.showClaseUnic = true;
                vm.showClases = false;
                vm.claseUnic = {
                  "id": vm.clases[0].id,
                  "name": vm.clases[0].matterCommunity.name + " " + vm.clases[0].section.name,
                  "sectionId": vm.clases[0].section.id
                };
                vm.asignClase(vm.claseUnic);
              }else{
                $timeout(function(){
                  vm.terminate = true;
                }, 1000);
                vm.showClaseUnic = false;
                vm.showClases = false;
                vm.showMessageInitial = true;
                vm.messageClaseShow = true;
                vm.messageClase = "No posees clases asociadas en esta comunidad. ¡Para asociar una clase haga click aquí!";
                vm.showHelp = true;
                vm.helpMessage = "No posees clases asociadas en esta comunidad. ¡Para asociar una clase haga click aquí!";
              }
            }
            vm.message = "";
          },
          function (error) {
            vm.showErrorMessage();
            vm.message = "Believe or not person isn't at home, leave your message at the beep, it must be ot it would pick up the phone, where would it be? Believe or not person's not home...";
          }
        );
      }else{
        vm.terminate = true;
        vm.showMessageInitial = true;
      }
    };

    vm.ok = function () {
      $scope.modalInstance.dismiss('cancel');
    };

    vm.asignClase = function (clase) {
      vm.claseSelects = clase;
      StudentsBySection.get({id: vm.claseSelects.sectionId},
        function (success) {
          vm.respaldoEstudiantes = success.response;
          if(vm.respaldoEstudiantes.length > 0){
            vm.show = true;
            vm.showHelp=false;
            for (var i = 0; i < vm.respaldoEstudiantes.length; i++) {
              vm.respaldoEstudiantes[i].assistance = true;
            }
            vm.dates = vm.date.getDate() + "-" + (vm.date.getMonth() + 1) + "-" + vm.date.getFullYear();
            AssistanceStudents.get({matterCommunityId: vm.claseSelects.id, date: vm.dates},
              function (success) {
                vm.asistenciaEstudiantes = success.response.userAssistList;
                if (vm.asistenciaEstudiantes.length > 0) {
                  vm.estudiantes = success.response.userAssistList;
                  vm.edit = true;
                } else {
                  vm.estudiantes = vm.respaldoEstudiantes;
                  vm.edit = false;
                }
                $timeout(function(){
                  vm.terminate = true;
                }, 1000);
              },
              function (error) {
                vm.mensajeError = "¡Oops! Algo ha salido mal";
                $scope.modalInstanceError = $uibModal.open({
                  templateUrl: 'modules/modals/partials/modal-error.html',
                  scope: $scope,
                  size: 'sm',
                  keyboard  : false
                });
                vm.showErrorMessage();
                vm.message = "Believe or not person isn't at home, leave your message at the beep, it must be ot it would pick up the phone, where would it be? Believe or not person's not home..."
              })
            }else{
              $timeout(function(){
                vm.terminate = true;
              }, 1000);
              vm.estudiantes = null;
              vm.showClaseUnic = false;
              vm.showHelp=true;
              vm.helpMessage="En la clase "+clase.name+" no hay estudiantes. ¡Para asociar estudiantes haga click aquí!";
            }
          },
          function (error) {
            vm.mensajeError = "¡Oops! Algo ha salido mal";
            $scope.modalInstanceError = $uibModal.open({
              templateUrl: 'modules/modals/partials/modal-error.html',
              scope: $scope,
              size: 'sm',
              keyboard  : false
            });
            vm.showErrorMessage();
            vm.message = "Believe or not person isn't at home, leave your message at the beep, it must be ot it would pick up the phone, where would it be? Believe or not person's not home..."
          });
    };

    vm.buscarAsistenciaPorFecha = function (date) {
      if(date != undefined || date != null){
        vm.showStudentsMessage=false;
        vm.dates = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        AssistanceStudents.get({matterCommunityId: vm.claseSelects.id, date: vm.dates},
            function (success) {
              vm.asistenciaEstudiantes = success.response.userAssistList;
              vm.estudiantes = [];
              $timeout(function(){
                 if (vm.asistenciaEstudiantes.length > 0) {
                   vm.estudiantes = success.response.userAssistList;
                   vm.edit = true;
                 } else {
                   vm.estudiantes = vm.respaldoEstudiantes;
                   vm.edit = false;
                 }
              }, 1000);
            },
            function (error) {
              vm.mensajeError = "¡Oops! Algo ha salido mal";
              $scope.modalInstanceError = $uibModal.open({
                templateUrl: 'modules/modals/partials/modal-error.html',
                scope: $scope,
                size: 'sm',
                keyboard  : false
              });
              vm.showErrorMessage();
              vm.message = "Believe or not person isn't at home, leave your message at the beep, it must be ot it would pick up the phone, where would it be? Believe or not person's not home..."
            })
           }else{
        vm.showStudentsMessage=true;
        vm.estudiantes=null;
        vm.studentsMessage = "Fecha seleccionada no es el dia actual o un dia anterior";
      }
    };

    vm.guardarAsistencia = function () {
      vm.showSuccessAssistance = false;
      vm.showErrorAssistance = false;
      vm.assistencia = {"date": null, "matterCommunitySectionId": null, "userAssistList": []};
      vm.assistencia.matterCommunitySectionId = vm.claseSelects.id;
      for (var i = 0; i < vm.estudiantes.length; i++) {
        vm.estudiante = {"userId": vm.estudiantes[i].id, "assistance": vm.estudiantes[i].assistance};
        vm.assistencia.userAssistList.push(vm.estudiante);
      }
      vm.assistencia.date = vm.date.getDate() + "-" + (vm.date.getMonth() + 1) + "-" + vm.date.getFullYear();
      AssistanceStudents.save({matterCommunityId: vm.claseSelects.id}, vm.assistencia,
          function (success) {
            vm.edit = true;
            vm.showSuccessAssistance = true;
            vm.successMessage = vm.assistanceSavedSuccess;
            $timeout(function(){
              vm.showSuccessAssistance = false;
            }, 2500);
            vm.mensajeSuccessful = "¡Felicitaciones! La asistencia se guardo satisfactoriamente";
            $scope.modalInstanceSuccess = $uibModal.open({
              templateUrl: 'modules/modals/partials/modal-succefull.html',
              scope: $scope,
              size: 'sm',
              keyboard  : false
            });
          },
          function (error) {
            vm.mensajeError = "¡Oops! Algo ha salido mal";
            $scope.modalInstanceError = $uibModal.open({
              templateUrl: 'modules/modals/partials/modal-error.html',
              scope: $scope,
              size: 'sm',
              keyboard  : false
            });
            vm.showErrorMessage();
          })
    };

    vm.editarAsistencia = function (estudiante) {
      vm.showSuccessAssistance = false;
      vm.showErrorAssistance = false;
      vm.assistencia = {"date": null, "matterCommunitySectionId": null, "userAssistList": []};
      vm.assistencia.matterCommunitySectionId = vm.claseSelects.id;
      vm.assistencia.userAssistList.push(estudiante);
      vm.assistencia.date = vm.date.getDate() + "-" + (vm.date.getMonth() + 1) + "-" + vm.date.getFullYear();
      AssistanceStudentsEdit.update({}, vm.assistencia,
          function (success) {
            vm.showSuccessAssistance = true;
            vm.successMessage = vm.assistanceSavedSuccess;
            vm.mensajeSuccessful = "¡Felicitaciones! La asistencia se guardo satisfactoriamente";
            $scope.modalInstanceSuccess = $uibModal.open({
              templateUrl: 'modules/modals/partials/modal-succefull.html',
              scope: $scope,
              size: 'sm',
              keyboard  : false
            });
          },
          function (error) {
            vm.mensajeError = "¡Oops! Algo ha salido mal";
            $scope.modalInstanceError = $uibModal.open({
              templateUrl: 'modules/modals/partials/modal-error.html',
              scope: $scope,
              size: 'sm',
              keyboard  : false
            });
            vm.showErrorMessage();
          })
    };

    vm.editarAsistencias = function () {
      vm.showSuccessAssistance = false;
      vm.showErrorAssistance = false;
      vm.assistencia = {"date": null, "matterCommunitySectionId": null, "userAssistList": []};
      vm.assistencia.matterCommunitySectionId = vm.claseSelects.id;
      for (var i = 0; i < vm.estudiantes.length; i++) {
        vm.estudiante = {"userId": vm.estudiantes[i].id, "assistance": vm.estudiantes[i].assistance};
        vm.assistencia.userAssistList.push(vm.estudiante);
      }
      vm.assistencia.date = vm.date.getDate() + "-" + (vm.date.getMonth() + 1) + "-" + vm.date.getFullYear();
      AssistanceStudentsEdit.update({}, vm.assistencia,
          function (success) {
            vm.showSuccessAssistance = true;
            vm.successMessage = vm.assistanceSavedSuccess;
            vm.mensajeSuccessful = "¡Felicitaciones! La asistencia se guardo satisfactoriamente";
            $scope.modalInstanceSuccess = $uibModal.open({
              templateUrl: 'modules/modals/partials/modal-succefull.html',
              scope: $scope,
              size: 'sm',
              keyboard  : false
            });
          },
          function (error) {
            vm.mensajeError = "¡Oops! Algo ha salido mal";
            $scope.modalInstanceError = $uibModal.open({
              templateUrl: 'modules/modals/partials/modal-error.html',
              scope: $scope,
              size: 'sm',
              keyboard  : false
            });
            vm.showErrorMessage();
          })
    };

    vm.typeClass = function (item) {
      if (item) {
        vm.integral = "INTEGRAL";
      } else {
        vm.integral = "NON_INTEGRAL";
      }
      ComunidadMateriasByType.get({
            communityId: vm.communitySelected.communityOrigin.id,
            roleId: 1,
            userId: vm.user.id,
            type: vm.integral
          },
          function (success) {
            vm.clases = success.response;
            vm.clase = [];
            vm.show = false;
            vm.estudiantes = [];
            vm.seleccion = true;
            for (var i = 0; i < vm.clases.length; i++) {
              vm.materSeccion = {
                "id": vm.clases[i].id,
                "name": vm.clases[i].matterCommunity.name + " " + vm.clases[i].section.name,
                "sectionId": vm.clases[i].section.id
              };
              vm.clase.push(vm.materSeccion)
            }
            $timeout(function(){
              vm.terminate = true;
            }, 1000);
          }, function (error) {
            vm.showErrorMessage();
          });
    };

    vm.cerrar = function(){
      $scope.modalInstanceSuccess.dismiss('cancel');
    };

    vm.salir = function(){
      $scope.modalInstanceError.dismiss('cancel');
    };

    //escucha cuando exista cambio en la comunidad seleccionada
    $scope.$on('community', function (evt, community) {
      vm.communitySelected = community;
      vm.messageInitial = "Por favor seleccione una comunidad";
      vm.loadInitial();
    });

    //Valida si el usuario esa autenticado con los permisos para la vista
    if (permission === null || authentication.getUser() === null) {
      $state.go('root');
      console.log("no esta autenticado:" + permission + " JSON:" + permission !== undefined ? JSON.stringify(permission) : "null");
    } else {
      // carga inicial por defecto
      vm.loadInitial();
    }
  }
})();
