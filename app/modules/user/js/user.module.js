(function(){
    'use strict';

    angular
        .module("sckola.user", ['ui.router'])
        .run(addStateToScope)
        .config(getRoutes);


    addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
    function addStateToScope($rootScope, $state, $stateParams){
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    };

    getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function getRoutes($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('editProfile', {
                url: '/edit/profile',
                parent: 'index',
                views: {

                  'navbar@': {
                            templateUrl: 'modules/navbar/partials/navbar.html',
                            controller: 'NavBarCtrl',
                            controllerAs: 'vm'
                          },
                  'sidemenu@':{
                            templateUrl: 'modules/sidemenu/partials/sidemenu.html',
                            controller: 'SideMenuCtrl',
                            controllerAs: 'vm'
                          },
                    'notifications@': {
                        templateUrl: 'modules/notification/partials/notification.html',
                        controller: 'NotificationCtrl',
                        controllerAs: 'vm'
                    },
                  'content@':{
                            templateUrl: 'modules/user/partials/editProfile.html',
                            controller: 'EditProfileCtrl',
                            controllerAs: 'vm'
                          },
                  'footer@': {
                              templateUrl: '',
                              controller: '',
                              controllerAs: ''
                            },
                    'news@': {
                        templateUrl: 'modules/news/partials/news.html',
                        controller: 'NewsCtrl',
                        controllerAs: 'vm'
                            }

                }

            })

    };
})();
