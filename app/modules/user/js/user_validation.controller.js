(function(){
  'use strict';

  angular
    .module('sckola.user')
    .controller('ValidateUserRegistrationCtrl', ValidateUserRegistrationCtrl);

  ValidateUserRegistrationCtrl.$inject = ['$scope','$location','ValidateUserRegistration','$state','$timeout'];
  function ValidateUserRegistrationCtrl($scope,$location,ValidateUserRegistration,$state,$timeout){
    var vm = this;
    vm.code = $location.absUrl().split('=')[1];
    vm.tokenLoadMessage = true;
    vm.message = "Estamos activando su cuenta de SKOLA ...";
    $timeout(function() {

      ValidateUserRegistration.get({code: vm.code},
        function(success){
          vm.message = "Cuenta activa";



          $timeout(function() {
            $state.go('root');
          }, 5000);
        },
        function(error){
          vm.message = "Hubo problemas al validar su cuenta";


          $timeout(function() {
            $state.go('root');
          }, 5000);
        });
      }, 5000);

    vm.ok = function () {
        $scope.modalInstance.dismiss ('cancel');
    };
  }

})();
