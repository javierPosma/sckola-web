(function(){
    'use strict';

    angular
        .module('sckola')
        .controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['$scope', '$modal', '$state'];
    function HomeCtrl ($scope, $modal, $state){
        var vm = this;
        vm.message = "mensaje inicial HOLA MUNDO :)";
        //Configuracion con la applicacion de facebook Connect
        /*window.fbAsyncInit = function() {
            FB.init({
                appId            : '1782384328492412',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v3.1'
            });
        };
        //SDK DE FACEBOOK
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/es_LA/sdk.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));*/

        vm.matter = function(){
            //alert ('antes de stateGo');
            $state.go('matterHome');
        };
    }
})();
