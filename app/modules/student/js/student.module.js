(function(){
    'use strict';

    angular
        .module("sckola.students", ['ui.router','ui.bootstrap'])
        .run(addStateToScope)
        .config(getRoutes);


    addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
    function addStateToScope($rootScope, $state, $stateParams){
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    };

    getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function getRoutes($stateProvider, $urlRouterProvider){

        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('students', {
                parent: 'index',
                url: '/students',
                views:{
                    'navbar@': {
                        templateUrl: 'modules/navbar/partials/navbar.html',
                        controller: 'NavBarCtrl',
                        controllerAs: 'vm'
                    },
                    'sidemenu@':{
                        templateUrl: 'modules/sidemenu/partials/sidemenu.html',
                        controller: 'SideMenuCtrl',
                        controllerAs: 'vm'
                    },
                    'notifications@': {
                        templateUrl: 'modules/notification/partials/notification.html',
                        controller: 'NotificationCtrl',
                        controllerAs: 'vm'
                    },
                    'content@': {
                        templateUrl: "modules/student/partials/listStudents.html",
                        controller: "StudentCTRL",
                        controllerAs: 'vm'
                    },
                    'footer@': {
                        templateUrl: '',
                        controller: '',
                        controllerAs: ''
                    },
                    'news@': {
                        templateUrl: 'modules/news/partials/news.html',
                        controller: 'NewsCtrl',
                        controllerAs: 'vm'
                    }
                }

            })
            .state('studentsNew', {
                parent: 'index',
                url: '/students/new',
                views:{
                    'navbar@': {
                        templateUrl: 'modules/navbar/partials/navbar.html',
                        controller: 'NavBarCtrl',
                        controllerAs: 'vm'
                    },
                    'sidemenu@':{
                        templateUrl: 'modules/sidemenu/partials/sidemenu.html',
                        controller: 'SideMenuCtrl',
                        controllerAs: 'vm'
                    },
                    'notifications@': {
                        templateUrl: 'modules/notification/partials/notification.html',
                        controller: 'NotificationCtrl',
                        controllerAs: 'vm'
                    },
                    'content@': {
                        templateUrl: "modules/student/partials/newStudents.html",
                        controller: "StudentCTRL",
                        controllerAs: 'vm'
                    },
                    'footer@': {
                        templateUrl: '',
                        controller: '',
                        controllerAs: ''
                    },
                    'news@': {
                        templateUrl: 'modules/news/partials/news.html',
                        controller: 'NewsCtrl',
                        controllerAs: 'vm'
                    }
                }

            })

    };
})();
