(function(){
  'use strict';

  angular.module('sckola')
  .controller('NavBarCtrl',NavBarCtrl);

  NavBarCtrl.$inject = ['$scope','$state','CommunityUser','authentication','NotificationHistory','$rootScope', 'NewsWizard', '$timeout', 'NotificationInfoService', '$uibModal'];
  function NavBarCtrl($scope,$state,CommunityUser,authentication,NotificationHistory, $rootScope, NewsWizard, $timeout, NotificationInfoService, $uibModal){
    var vm = this;
    var permission = authentication.currentUser();
    var sumProgress = 0;
      vm.mensage = "No hay notificaciones pendientes";

    vm.user = authentication.getUser();
    CommunityUser.get({userId:vm.user.id,roleId:1},null,
        function(success){
            if (success && success.response!= null && success.response.length>0) {
                vm.communitiesUser = success.response;
                if(authentication.getCommunity() == undefined){
                  vm.communitySelected = vm.communitiesUser[0];
                  vm.sendEvent(vm.communitySelected);
                }else{
                    vm.communitySelected = authentication.getCommunity();
                }
            }
        },
        function(error){
            vm.message = "Error cargando mis comunidades"
        });


    vm.loadInitial = function(){
      vm.news = [];
      vm.notifications = 0;
      vm.showNotificationCount = false;
      vm.activeCommunity = authentication.getCommunity();
        NotificationInfoService.get(
            function(success){
                vm.news = success.response.notifications;
                vm.notifications += success.response.activeNotifications;
                if(vm.activeCommunity != null){
                    NotificationHistory.get({communityId: vm.activeCommunity.communityOrigin.id},
                        function(success){
                            for(var a=0;a < success.response.notifications.length; a++){
                                vm.news.push(success.response.notifications[a]);
                            }
                            vm.notifications += success.response.activeNotifications;
                        },function(error){

                        })
                }
            });
    };

      vm.resetCount = function(){
          vm.notifications = 0;
      };

      vm.goToProfile = function(){
          $state.go("editProfile");
      };

      vm.goHome = function(){
          $state.go('dashboard');
      };

      vm.goToCommunities = function(){
        $state.go('communities');
      };

      vm.goToCourses = function(){
        $state.go('matterHome');
      };

      vm.goToAssistances = function(){
        $state.go("assistances");
      };

      vm.goToStudents = function(){
        $state.go("students");
      };

      vm.gotToEvaluations = function(){
        $state.go("evaluationPlan");
      };

      vm.goToUploadNotes = function(){
        $state.go("uploadNotes");
      };

      vm.goToRoot = function(){
          $scope.modalInstance = $uibModal.open({
              templateUrl: 'modules/modals/partials/modal.html',
              scope: $scope,
              //backdrop: 'static',
              windowClass: 'modal-position',
              size: 'sm',
              keyboard: false
          });
      };

      vm.cerrar = function(){
          $scope.modalInstance.dismiss('cancel');
      };

      vm.salir = function(){
           $rootScope.tokenLoging = undefined;
           $state.go("root");
           $scope.modalInstance.dismiss('cancel');
      };

      vm.setCommunitySelected = function(community){
        vm.communitySelected = community;
        vm.sendEvent(vm.communitySelected);
      };

      vm.sendEvent = function(communitySelected) {
        authentication.setCommunity(communitySelected);
        $scope.$parent.$broadcast('community', communitySelected);
      };


      $scope.$on('community', function (evt, community) {
          vm.activeCommunity = community;
          vm.messageInitial = "Por favor seleccione una comunidad";
          vm.loadInitial();
      });

      $scope.$on('newCommunity', function(evt, community){
        vm.communitiesUser.push(community);
      });

      var bar = new ProgressBar.Line("#progress", {
              strokeWidth: 4,
              easing: 'easeInOut',
              duration: 1400,
              color: '#75ABDD',
              trailColor: '#eee',
              trailWidth: 1,
              svgStyle: {width: '100%', height: '100%'},
              text: {
                  style: {
                      // Text color.
                      // Default: same as stroke color (options.color)
                      color: '#FFFFFF',
                      position: 'absolute',
                      right: '0',
                      padding: 0,
                      margin: 0,
                      transform: null
                  },
                  autoStyleContainer: false
              },
              from: {color: '#aaa'},
              to: {color: '#75ABDD'},
              step: function(state, bar) {
              bar.setText(Math.round(bar.value() * 100) + ' %');
            }
      });
      bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
      bar.text.style.fontSize = '2rem';
      //Para modificar el progreso hay que configurarlo entre 0.0 y 1.0
      //bar.animate(1.0);

      NewsWizard.get({userId: vm.user.id},
          function(success){

              vm.usuario = success.response;

              if(vm.usuario.profileUser != null){
                  sumProgress = sumProgress + 0.25;
              }
              if(vm.usuario.community != null){
                  sumProgress = sumProgress + 0.25;
              }
              if(vm.usuario.evaluationPlan != null){
                  sumProgress = sumProgress + 0.25;
              }
              if(vm.usuario.matterCommunitySection != null){
                  sumProgress = sumProgress + 0.25;
              }

              bar.animate(sumProgress);

          },
          function(error){

          });



      if (permission === null || authentication.getUser() === null) {
              // user is now logged out
              console.log("salio");
              $state.go('root');
          //$state.go('root');
          console.log("no esta autenticado:" + permission + " JSON:" + permission !== undefined ? JSON.stringify(permission) : "null");
      } else {
          //Loads initial data of the controller
          vm.loadInitial();
      }

      }

    })();
