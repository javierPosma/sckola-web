/**
* Created by posma.marialejandra on 28/03/2017.
* Controllers
* @namespace Controllers
*/
(function(){
  'use strict';

  angular
  .module('sckola.matter')
  .controller('MatterCtrl', MatterCtrl);

  MatterCtrl.$inject = ['authentication','$rootScope','$scope','$uibModal','$state','Communities','ComunidadMaterias','Matter','SectionByCommunity', 'AssociateMateriaSection', 'AssociateMateriaCommunity', 'CreateSection', 'MattersCommunity', 'DesassociateMatterCommunitySection','$timeout'];
  /**
  * @namespace MatterController
  * @desc Controller for Matters
  * @memberOf Controllers
  */
  function MatterCtrl(authentication,$rootScope,$scope,$uibModal,$state,Communities,ComunidadMaterias,Matter,SectionByCommunity, AssociateMateriaSection, AssociateMateriaCommunity, CreateSection, MattersCommunity, DesassociateMatterCommunitySection,$timeout) {
      var vm = this;
      vm.messageInitial = "Por favor seleccione una comunidad";
      vm.showMessageInitial = true;
      vm.showError = false;
      vm.errorMessage = "¡Oops! Algo ha salido mal";
      vm.disabledSeccionNew = false;
      vm.staticCommunity = [];
      vm.nameSection = null;
      vm.sectionIntegral = null;
      vm.terminate = false;
      vm.messageHelp = "";
      vm.showHelp = false;
      vm.section = false;
      vm.sectionLabel = false;
      vm.matter = null;
      vm.materia = false;
      vm.matterGeneral = false;
      vm.newSection = false;
      vm.showSuccessMessage = false;
      vm.newMatterMessage = "¡Felicidades! Has Creado una nueva materia";
      vm.removeMatterMessage = "¡Materia Eliminada con Éxito!";
      vm.successMessage = vm.newMatterMessage;
      vm.user = authentication.getUser();
      vm.validarCommunitySelect = true;
      vm.validarCommunityNew = true;
      vm.validarMatter = true;
      vm.validarTypeSeccion = true;
      vm.validarSection = true;
      vm.validarSectionIn = true;
      vm.validarSectionCommunity = true;

      Communities.get({},
          function(success){
              vm.staticCommunity = success.response;
              vm.loadInitial();
          },
          function(error){
              vm.showErrorMessage();
          });

      vm.user = authentication.getUser();
      vm.communitySelected = authentication.getCommunity();
      var permission = authentication.currentUser();
      $rootScope.tokenLoging = authentication.isLoggedIn();
      if(vm.communitySelected==null){
          vm.showHelp = true;
          vm.helpMessage = "Debes seleccionar una comunidad. ¡Para seleccionar una comunidad haga click aqui!";
      }
      /**
       * @name showErrorMessage
       * @desc Shows error message to the user
       * @memberOf Controllers.MatterController
       */
      vm.showErrorMessage = function(){
          vm.showError = true;
          $timeout(function(){
              vm.showError = false;
          },2500);
      };

      /**
       * @name helpUser
       * @desc Depending of the user state, if it has a community or not, a mmater or not,
       *       the state of the view is changed to the requirement.
       * @memberOf Controllers.MatterController
       */
      vm.helpUser = function(){
          if(vm.communitySelected == null){
              $state.go("communities");
          }
      };

      /**
       * @name labelMatterModalTransition
       * @desc Manages transition in modal window for matter label
       * @memberOf Controllers.MatterController
       */

      vm.labelMatterModalTransition = function(){
          if(vm.matterGeneral){
              vm.matterGeneral = false;
              $timeout(function(){
                  vm.materia = false;
              },500);
          }
          else
          if(!vm.materia){
              vm.materia = true;
              $timeout(function(){
                  vm.matterGeneral = true;
              },500);
          }
      };

      /**
       * @name sectionTransition
       * @desc Manages transition between combo and textbox for asociate a
       *       section to the matter
       * @memberOf Controllers.MatterController
       */
      vm.sectionTransition = function(){
          vm.sectionLabel = true;
          if(vm.seccion){
              vm.section = false;
              $timeout(function(){
                  vm.newSection = true;
              },500);
          }
          else{
              vm.newSection = false;
              $timeout(function(){
                  vm.section=true;
              },500);
          }
      };

      /**
       * @name loadInitial
       * @desc Loads initial data for the view depending of the selected community
       * @memberOf Controllers.MatterController
       */
      vm.loadInitial = function () {
          if (vm.communitySelected !== undefined && vm.communitySelected.id !== undefined) {
              vm.showMessageInitial = false;
              ComunidadMaterias.get({communityId: vm.communitySelected.communityOrigin.id, roleId: 1, userId: vm.user.id},
                  function (success) {
                      vm.communityMatters = success.response;
                      if(vm.communityMatters.length === 0){
                          vm.showHelp = true;
                          vm.helpMessage= "No posees clases asociadas en esta comunidad. ¡Haz click en el botón añadir para asociar una clase!";
                      }
                      vm.showMessageInitial = false;
                      $timeout(function(){
                          vm.terminate = true;
                      }, 1000);
                  },
                  function (error) {
                      vm.showErrorMessage();
                  }
              );

              MattersCommunity.get({communityId: vm.communitySelected.communityOrigin.id},
                  function (success) {
                      vm.mattersCommunitys = success.response;
                      if (vm.mattersCommunitys.length == 0) {
                          vm.matterExist = false;
                          vm.messageMatterLoad = "No existen materias asociadas a la comunidad";
                      }
                  },
                  function (error) {
                      vm.messageMatterLoad = "No existen materias asociadas a la comunidad";
                      if (error.errorCode == 'SK-002')
                          vm.matterExist = false;
                      vm.message = "Error en la carga de materias";
                      vm.showMessageInitial = true;
                  }
              );

              SectionByCommunity.get({communityId: vm.communitySelected.communityOrigin.id},
                  function (success) {
                      vm.sectionsByCommunity = success.response;
                  },
                  function (error) {
                      vm.showErrorMessage();
                  }
              )
          }else{
              vm.terminate = true;
              vm.showMessageInitial = true;
          }
      };

      /**
       * @name cancel
       * @desc Closes modal window
       * @memberOf Controllers.MatterController
       */
      vm.cancel = function () {
          $scope.modalInstance.dismiss('cancel');
      };

      /**
       * @name ok
       * @desc Closes modal window
       * @memberOf Controllers.MatterController
       */
      vm.ok = function () {
          $scope.modalInstance.dismiss('cancel');
      };

      /**
       * @name go
       * @desc Loads initial view for teacher
       * @memberOf Controllers.MatterController
       */
      vm.go = function () {
          $state.go('teacherHome');
      };

      /**
       * @name matterNew
       * @desc Open modal window to create a new matter
       * @memberOf Controllers.MatterController
       */
      vm.matterNew = function () {
          vm.communitySelect = null;
          vm.matterCommunity = null;
          vm.matter = null;
          vm.sectionByCommunity = null;
          vm.sectionIntegral = null;
          vm.seccion = null;
          vm.nameSection = null;
          vm.newSection = false;
          vm.section = false;
          vm.sectionLabel = false;
          vm.validarCommunitySelect = true;
          vm.validarCommunityNew = true;
          vm.validarMatter = true;
          vm.validarTypeSeccion = true;
          vm.validarSection = true;
          vm.validarSectionIn = true;
          vm.validarSectionCommunity = true;
          Matter.get({},
              function (success) {
                  vm.matters = success.response;
                  $scope.modalInstance = $uibModal.open({
                      templateUrl: 'modules/matter/partials/modal/newMatter.html',
                      scope: $scope,
                      size: 'md',
                      keyboard: false
                  });
              },
              function (error) {
                  vm.showErrorMessage();
              }
          )
      };

      /**
       * @name saveMatter
       * @desc Calls the service that saves matters
       * @memberOf Controllers.MatterController
       */
      vm.saveMatter = function () {
          vm.associate = {'associate': true};
          vm.sectionType = {"type": vm.sectionIntegral};
          if (vm.materia) {
              AssociateMateriaCommunity.save({matterId: vm.matter.id, communityId: vm.communitySelected.communityOrigin.id}, vm.associate,
                  function (success) {
                      vm.matterCommunityNew = success.response;
                      if (vm.seccion) {
                          vm.newSection = {"name": vm.nameSection, "comunityId": vm.communitySelected.communityOrigin.id};
                          CreateSection.save({communityId: vm.communitySelected.communityOrigin.id, userId: vm.user.id}, vm.newSection,
                              function (success) {
                                  AssociateMateriaSection.save({
                                          matterCommunityId: vm.matterCommunityNew.id,
                                          sectionId: success.response.id,
                                          userId: vm.user.id
                                      }, vm.sectionType,
                                      function (success) {
                                          vm.sectionIntegral = null;
                                          vm.seccion = null;
                                          vm.nameSection = null;
                                          vm.successMessage = vm.newMatterMessage;
                                          vm.showHelp = false;
                                          vm.showSuccessMessage = true;
                                          $timeout(function(){
                                              vm.showSuccessMessage = false;
                                          }, 2500);
                                          vm.communityMatters.push(success.response);
                                          $scope.modalInstance.dismiss('cancel');
                                          vm.mensajeSuccessful = "¡Felicidades! Has Creado una nueva materia";
                                          $scope.modalInstanceSuccess = $uibModal.open({
                                              templateUrl: 'modules/modals/partials/modal-succefull.html',
                                              scope: $scope,
                                              size: 'sm',
                                              keyboard  : false
                                          });
                                      },
                                      function (error) {
                                          vm.message = "Error en la creacion de clases";
                                          vm.mensajeError = "Error en la creacion de clases";
                                          $scope.modalInstanceError = $uibModal.open({
                                              templateUrl: 'modules/modals/partials/modal-error.html',
                                              scope: $scope,
                                              size: 'sm',
                                              keyboard  : false
                                          });
                                          vm.showErrorMessage();
                                      });
                              },
                              function (error) {
                                  vm.mensajeError = "Error en la creacion de clases";
                                  $scope.modalInstanceError = $uibModal.open({
                                      templateUrl: 'modules/modals/partials/modal-error.html',
                                      scope: $scope,
                                      size: 'sm',
                                      keyboard  : false
                                  });
                                  vm.showErrorMessage();
                              })
                      } else {
                          AssociateMateriaSection.save({
                                  matterCommunityId: vm.matterCommunityNew.id,
                                  sectionId: vm.sectionByCommunity.id,
                                  userId: vm.user.id
                              }, vm.sectionType,
                              function (success) {
                                  vm.sectionIntegral = null;
                                  vm.seccion = null;
                                  vm.sectionByCommunity = null;
                                  vm.successMessage = vm.newMatterMessage;
                                  vm.showSuccessMessage = true;
                                  vm.showHelp = false;
                                  $timeout(function(){
                                      vm.showSuccessMessage = false;
                                  }, 2500);
                                  vm.communityMatters.push(success.response);
                                  $scope.modalInstance.dismiss('cancel');
                                  vm.mensajeSuccessful = "¡Felicidades! Has Creado una nueva materia";
                                  $scope.modalInstanceSuccess = $uibModal.open({
                                      templateUrl: 'modules/modals/partials/modal-succefull.html',
                                      scope: $scope,
                                      size: 'sm',
                                      keyboard  : false
                                  });
                              },
                              function (error) {
                                  vm.message = "Error en la creacion de clases";
                                  vm.mensajeError = "Error en la creacion de clases";
                                  $scope.modalInstanceError = $uibModal.open({
                                      templateUrl: 'modules/modals/partials/modal-error.html',
                                      scope: $scope,
                                      size: 'sm',
                                      keyboard  : false
                                  });
                                  vm.showErrorMessage();
                              });
                      }
                  },
                  function (error) {
                      vm.showErrorMessage();
                  });
          } else {
              if (vm.seccion) {
                  vm.newSection = {"name": vm.nameSection, "comunityId": vm.communitySelected.communityOrigin.id};
                  CreateSection.save({communityId: vm.communitySelected.communityOrigin.id, userId: vm.user.id}, vm.newSection,
                      function (success) {
                          AssociateMateriaSection.save({
                                  matterCommunityId: vm.matterCommunity.id,
                                  sectionId: success.response.id,
                                  userId: vm.user.id
                              }, vm.sectionType,
                              function (success) {
                                  vm.sectionIntegral = null;
                                  vm.seccion = null;
                                  vm.nameSection = null;
                                  vm.successMessage = vm.newMatterMessage;
                                  vm.showSuccessMessage = true;
                                  vm.showHelp = false;
                                  $timeout(function(){
                                      vm.showSuccessMessage = false;
                                  }, 2500);
                                  vm.communityMatters.push(success.response);
                                  $scope.modalInstance.dismiss('cancel');
                                  vm.mensajeSuccessful = "¡Felicidades! Has Creado una nueva materia";
                                  $scope.modalInstanceSuccess = $uibModal.open({
                                      templateUrl: 'modules/modals/partials/modal-succefull.html',
                                      scope: $scope,
                                      size: 'sm',
                                      keyboard  : false
                                  });
                              },
                              function (error) {
                                  vm.message = "Error en la creacion de clases";
                                  vm.mensajeError = "Error en la creacion de clases";
                                  $scope.modalInstanceError = $uibModal.open({
                                      templateUrl: 'modules/modals/partials/modal-error.html',
                                      scope: $scope,
                                      size: 'sm',
                                      keyboard  : false
                                  });
                                  vm.showErrorMessage();
                              });
                      },
                      function (error) {
                          vm.mensajeError = "Error en la creacion de clases";
                          $scope.modalInstanceError = $uibModal.open({
                              templateUrl: 'modules/modals/partials/modal-error.html',
                              scope: $scope,
                              size: 'sm',
                              keyboard  : false
                          });
                          vm.showErrorMessage();
                      })
              } else {
                  AssociateMateriaSection.save({
                          matterCommunityId: vm.matterCommunity.id,
                          sectionId: vm.sectionByCommunity.id,
                          userId: vm.user.id
                      }, vm.sectionType,
                      function (success) {
                          vm.sectionIntegral = null;
                          vm.showHelp = false;
                          vm.seccion = null;
                          vm.sectionByCommunity = null;
                          vm.successMessage = vm.newMatterMessage;
                          vm.showSuccessMessage = true;
                          $timeout(function(){
                              vm.showSuccessMessage = false;
                          }, 2500);
                          vm.communityMatters.push(success.response);
                          $scope.modalInstance.dismiss('cancel');
                          vm.mensajeSuccessful = "¡Felicidades! Has Creado una nueva materia";
                          $scope.modalInstanceSuccess = $uibModal.open({
                              templateUrl: 'modules/modals/partials/modal-succefull.html',
                              scope: $scope,
                              size: 'sm',
                              keyboard  : false
                          });
                      },
                      function (error) {
                          vm.message = "Error en la creacion de clases";
                          vm.mensajeError = "Error en la creacion de clases";
                          $scope.modalInstanceError = $uibModal.open({
                              templateUrl: 'modules/modals/partials/modal-error.html',
                              scope: $scope,
                              size: 'sm',
                              keyboard  : false
                          });
                          vm.showErrorMessage();
                      });
              }
          }
      };

      /**
       * @name editMatter
       * @desc Closes modal window
       * @param {object} object matter
       * @memberOf Controllers.MatterController
       */
      vm.editMatter = function (object) {
          $scope.modalInstance = $uibModal.open({
              templateUrl: 'modules/matter/partials/modal/newMatter.html',
              scope: $scope,
              size: 'md',
              keyboard: false
          });
      };

      /**
       * @name matterCommunitySectionDisassociate
       * @desc Desassociate a matter from a community section
       * @param {object} matterCommunitySection matter to desassociate
       * @memberOf Controllers.MatterController
       */
      vm.matterCommunitySectionDisassociate = function(matterCommunitySection){
          vm.associate={associate: false};
          DesassociateMatterCommunitySection.update({
                  matterCommunitySectionId:matterCommunitySection.id
              },vm.associate,
              function(success){
                  var indexOfMatter = vm.communityMatters.indexOf(matterCommunitySection);
                  vm.communityMatters.splice(indexOfMatter, 1);
                  vm.showSuccessMessage = true;
                  vm.successMessage = vm.removeMatterMessage;
                  $timeout(function(){
                      vm.showSuccessMessage = false;
                  }, 2500);
                  vm.mensajeSuccessful = "¡Materia Eliminada con Éxito!";
                  $scope.modalInstanceSuccess = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-succefull.html',
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                  });
              });
      };

      //escucha cuando exista cambio en la comunidad seleccionada
      $scope.$on('community', function (evt, community) {
          vm.communitySelected = community;
          vm.loadInitial();
      });

      //Valida si el usuario esa autenticado con los permisos para la vista
      if (permission === null || authentication.getUser() === null) {
          $state.go('root');
          console.log("no esta autenticado:" + permission + " JSON:" + permission !== undefined ? JSON.stringify(permission) : "null");
      } else {
      // carga inicial por defecto
      vm.loadInitial();
      }

      vm.validarClase = function(){
          if(vm.communitySelect == null || vm.communitySelect == ""){
              vm.validarCommunitySelect = true;
          }else{
              vm.validarCommunitySelect = false;
          }
      }

      vm.validarClaseNew = function(){
          if(vm.matterCommunity == null || vm.matterCommunity == ""){
              vm.validarCommunityNew = true;
          }else{
              vm.validarCommunityNew = false;
          }
      }

      vm.validarClaseOld = function(){
          if(vm.matter == null || vm.matter == ""){
              vm.validarMatter = true;
          }else{
              vm.validarMatter = false;
          }
      }

      vm.validarTipoSeccion = function(){
          if(vm.seccion == null || vm.seccion == "" || (vm.seccion != false && vm.seccion != true)){
              vm.validarTypeSeccion = true;
          }else{
              vm.validarTypeSeccion = false;
          }
      };

      vm.validarSectionIntegral = function(){
          if(vm.sectionIntegral == null || vm.sectionIntegral == ""){
              vm.validarSection = true;
          }else{
              vm.validarSection = false;
          }
      };

      vm.validarSectionselect = function(){
          if(vm.sectionByCommunity == null || vm.sectionByCommunity == ""){
              vm.validarSectionCommunity = true;
          }else{
              vm.validarSectionCommunity = false;
          }
      };

      vm.validarSectioninput = function(){
          if(vm.nameSection == null || vm.nameSection == ""){
            vm.validarSectionIn = true;
          }else{
              vm.validarSectionIn = false;
          }
      }

      vm.cerrar = function(){
          $scope.modalInstanceSuccess.dismiss('cancel');
      };

      vm.salir = function(){
          $scope.modalInstanceError.dismiss('cancel');
      }
  }
})();
