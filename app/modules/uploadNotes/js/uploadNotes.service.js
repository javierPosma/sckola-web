(function(){

    'use strict';

    angular.module('sckola.uploadNotes')
        .factory('CreateQualificationByMatterSectionByEvaluation', createQualificationByMatterSectionByEvaluation)
        .factory('QualificationByStudentEdit', qualificationByStudentEdit)
        .factory('QualificationByStudent', qualificationByStudent)
        .factory('EvaluationCulminated', evaluationCulminated);

    //Servicio que crea las calificaciones de todos los estudiantes de una materia comunity section a una evaluacion
    createQualificationByMatterSectionByEvaluation.$inject = ['$resource','$rootScope'];
    function createQualificationByMatterSectionByEvaluation($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/qualification/matter_community_section/:matterId/evaluation/:evaluationId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

    //Servicio que edit una calificacion de un estudiante de una evaluacion
    qualificationByStudentEdit.$inject = ['$resource','$rootScope'];
    function qualificationByStudentEdit($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/qualification/:qualificationId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

    //Servicio que Buscar una calificacion de un estudiante de una evaluacion
    qualificationByStudent.$inject = ['$resource','$rootScope'];
    function qualificationByStudent($resource,$rootScope){
        return $resource($rootScope.domainUrl+'skola/qualification/matter_community_section/:matterId/user/:userId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

    //Servicio que cambia el estatus de la evaluacion a culminada
    evaluationCulminated.$inject = ['$resource','$rootScope'];
    function evaluationCulminated($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/:evaluationId/culminated',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };


})();