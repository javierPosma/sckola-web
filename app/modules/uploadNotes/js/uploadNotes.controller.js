(function(){
    'use strict';

    angular
        .module('sckola.uploadNotes')
        .controller('UploadNotesCTRL', uploadNotesCTRL);

    uploadNotesCTRL.$inject = ['authentication', '$scope', '$rootScope', '$state', 'EvaluationByMatterCommunitySection', 'ComunidadMaterias', 'CreateQualificationByMatterSectionByEvaluation', 'QualificationByStudentEdit', 'QualificationByStudent', 'EvaluationCulminated', '$timeout', '$uibModal'];
    function uploadNotesCTRL(authentication, $scope, $rootScope, $state, EvaluationByMatterCommunitySection, ComunidadMaterias, CreateQualificationByMatterSectionByEvaluation, QualificationByStudentEdit, QualificationByStudent, EvaluationCulminated, $timeout, $uibModal) {
        var vm = this;
        vm.message = "cargando pagina";
        vm.messageInitial = "Por favor seleccione una comunidad";
        vm.helpMessage = "";
        vm.noStudentMessage = "No existen estudiantes asociados a la sección, ¡Para asociar estudiantes a la sección haga click aquí!";
        vm.clase = [];
        vm.estudiantes = [];
        vm.evaluaciones = [];
        vm.siguiente = false;
        vm.show = false;
        vm.notas = true;
        vm.showHelp = false;
        vm.showMessageInitial=true;
        vm.showErrorMessageModal = false;
        vm.showSuccessMessageModal = false;
        vm.showErrorMessage = false;
        vm.showSuccessMessage = false;
        //alert("$rootScope.tokenLoging 1: "+$rootScope.tokenLoging)
        //vm.logged = vm.isLoggedIn();
        $rootScope.tokenLoging = true;
        //alert("$rootScope.tokenLoging 2: "+$rootScope.tokenLoging)

        vm.user = authentication.getUser();
        vm.communitySelected = authentication.getCommunity();
        if(vm.communitySelected==null){
          vm.showHelp = true;
          vm.helpMessage = "Debes seleccionar una comunidad. ¡Para seleccionar una comunidad haga click aqui!";
        }
        var permission = authentication.currentUser();
        $rootScope.tokenLoging = authentication.isLoggedIn();


        /**
        * @name helpUser
        * @desc Depending of the user state, if it has a community or not, a mmater or not,
        *       the state of the view is changed to the requirement.
        * @memberOf Controllers.UploadNoteController
        */
        vm.helpUser = function(){
          if(vm.communitySelected == null){
            $state.go("communities");
          }
          else if(vm.clases.length < 1){
            $state.go("matterHome");
          }
          else if(vm.evaluaciones.length <1){
            $state.go("evaluationPlan");
          }
          else {
            $state.go("students");
          }
        };

        vm.loadInitial = function () {
                vm.showMessageInitial = false;
                vm.show=false;
                if (vm.communitySelected !== undefined && vm.communitySelected.id !== undefined) {
                ComunidadMaterias.get({communityId: vm.communitySelected.communityOrigin.id, roleId: 1, userId: vm.user.id},
                    function (success) {
                        vm.clases = success.response;
                        vm.messageClaseShow = false;
                        if(vm.clases.length > 1){
                            vm.showClaseUnic = false;
                            vm.clase = [];
                            for (var i = 0; i < vm.clases.length; i++) {
                                vm.materSeccion = {
                                    "id": vm.clases[i].id,
                                    "name": vm.clases[i].matterCommunity.name + " " + vm.clases[i].section.name,
                                    "sectionId": vm.clases[i].section.id
                                };
                                vm.clase.push(vm.materSeccion)
                            }
                            $timeout(function(){
                                vm.terminate = true;
                            }, 1000);
                        }else{
                            if(vm.clases[0] != null){
                                vm.showClaseUnic = true;
                                vm.claseUnic = {
                                    "id": vm.clases[0].id,
                                    "name": vm.clases[0].matterCommunity.name + " " + vm.clases[0].section.name,
                                    "sectionId": vm.clases[0].section.id
                                };
                                vm.asignClase(vm.claseUnic);
                            }else{
                                $timeout(function(){
                                    vm.terminate = true;
                                }, 1000);
                                vm.showMessageInitial = true;
                                vm.messageClaseShow = true;
                                vm.messageClase = "No existen clases en la comunidad";
                                vm.showHelp = true;
                                vm.helpMessage = "No posees clases asociadas en esta comunidad. ¡Para asociar una clase haga click aquí!"
                            }
                        }
                        vm.message = "";
                    },
                    function (error) {
                        vm.message = "Believe or not person isn't at home, leave your message at the beep, it must be ot it would pick up the phone, where would it be? Believe or not person's not home...";
                    }
                );
              }else{
                vm.terminate = true;
                vm.showMessageInitial = true;
              }
        };

        vm.asignClase = function (clase) {
            vm.claseId = clase.id;
            vm.claseSelect = clase;
            vm.showHelp = false;
            EvaluationByMatterCommunitySection.get({matterId: vm.claseId},
                function (success) {
                    vm.messageClaseShow = false;
                    if (success.response != null && success.response != undefined) {
                        if (success.response.length > 1) {
                            vm.evaluaciones =[];
                            for (var b = 1; b <= success.response.length; b++) {
                                vm.evaluaciones.push(success.response[b]);
                            }
                            vm.siguiente = true;
                        }
                        vm.evaluation = success.response[0];
                        vm.messageClaseShow = false;

                        vm.escala = vm.evaluation.evaluationScale.evaluationValueList;
                        if (vm.evaluation.status == "PENDING") {
                            CreateQualificationByMatterSectionByEvaluation.save({
                                    matterId: vm.claseId,
                                    evaluationId: vm.evaluation.id
                                }, null,
                                function (success) {
                                    if(success.message != "Students no exists"){
                                        vm.estudiantes = [];
                                        vm.show = true;
                                        for (var i = 0; i < success.response.qualificationUserList.length; i++) {
                                            vm.estudiante = success.response.qualificationUserList[i].user;
                                            vm.estudiante.qualificationId = success.response.qualificationUserList[i].id;
                                            if (success.response.qualificationUserList[i].qualification != null) {
                                                vm.estudiante.qualification = success.response.qualificationUserList[i].qualification;
                                                vm.estudiante.validate = true;
                                            } else {
                                                vm.estudiante.qualification = null;
                                                vm.estudiante.validate = true;
                                            }
                                            vm.estudiantes.push(vm.estudiante);
                                        }
                                        $timeout(function(){
                                            vm.terminate = true;
                                        }, 2500);
                                    }else{
                                        $timeout(function(){
                                            vm.terminate = true;
                                        }, 2500);
                                        vm.show = false;
                                        vm.messageClaseShow = true;
                                        vm.showHelp = true;
                                        vm.helpMessage = vm.noStudentMessage;
                                        vm.messageClase = "No existen estudiantes en la clase"
                                    }
                                },
                                function (error) {

                                });
                        } else {
                            vm.estudiantes = [];
                            vm.contador = 0;
                            vm.show = true;
                            for (var i = 0; i < vm.evaluation.qualificationUserList.length; i++) {
                                vm.estudiante = vm.evaluation.qualificationUserList[i].user;
                                vm.estudiante.qualificationId = vm.evaluation.qualificationUserList[i].id;
                                vm.estudiante.qualification = vm.evaluation.qualificationUserList[i].qualification;
                                if (vm.estudiante.qualification != null) {
                                    vm.contador++;
                                }else{
                                    vm.estudiante.validate = true;
                                }
                                vm.estudiantes.push(vm.estudiante);
                            }
                            if (vm.contador == vm.estudiantes.length){
                                vm.notas = false;
                            }
                            $timeout(function(){
                                vm.terminate = true;
                            }, 2500);
                        }
                    }else{
                        vm.okButton = true;
                        vm.show = false;
                        if(success.message != null){
                            $timeout(function(){
                                vm.terminate = true;
                            }, 2500);
                            vm.showHelp = true;
                            vm.helpMessage = "La clase "+vm.claseSelect.name+" no posee un plan de evaluaci\u00F3n. ¡Para crear un plan de evaluación haga click aquí!";
                        }else{
                            $timeout(function(){
                                vm.terminate = true;
                            }, 2500);
                            vm.showHelp = true;
                            vm.show = false;
                            vm.helpMessage="La clase "+ vm.claseSelect.name +" no tiene ninguna evaluaci\u00F3n pendiente o en progreso";
                        }
                    }
                },
                function (error) {
                    vm.message = "Believe or not person isn't at home, leave your message at the beep, it must be ot it would pick up the phone, where would it be? Believe or not person's not home..."
                });
        };

        vm.ok = function () {
            $scope.modalInstance.dismiss('cancel');
        };

        vm.selectEstudiante = function (estudiante) {
            vm.estudiante = estudiante;
            vm.botonOk = true;
            QualificationByStudent.get({matterId: vm.claseId, userId: vm.estudiante.id},
                function (success) {
                    vm.calificationStudent = success.response;
                    $scope.modalInstance = $uibModal.open({
                        templateUrl: 'modules/uploadNotes/partials/modal/detailStudentNotes_modal.html',
                        scope: $scope,
                        //backdrop: 'static',
                        windowClass: 'modal-position',
                        size: 'md',
                        keyboard: false
                    });
                },
                function (error) {
                    vm.message = "Believe or not person isn't at home, leave your message at the beep, it must be ot it would pick up the phone, where would it be? Believe or not person's not home...";
                }
            );
        };

        vm.cargarNota = function (estudiante) {
            vm.estudianteNota = {"qualification": estudiante.qualification};
            vm.estudianteNota.qualification.weight = vm.evaluation.weight;
            QualificationByStudentEdit.update({qualificationId: estudiante.qualificationId}, vm.estudianteNota,
                function (success) {
                    vm.showSuccessMessage = true;
                    $timeout(function(){
                        vm.showSuccessMessage = false;
                    }, 2500);
                    vm.showErrorMessage = false;
                    vm.successMessage = "Felicitaciones, has cargado la nota del estudiante";
                    vm.mensajeSuccessful = "¡Felicitaciones, has cargado la nota del estudiante!";
                    $scope.modalInstanceSuccess = $uibModal.open({
                        templateUrl: 'modules/modals/partials/modal-succefull.html',
                        scope: $scope,
                        size: 'sm',
                        keyboard  : false
                    });
                },
                function (error) {
                    vm.showErrorMessage = true;
                    vm.showSuccessMessage = false;
                    $timeout(function(){
                        vm.showErrorMessage = false;
                    }, 2500);
                    vm.mensajeError = "Hubo un error al cargar la nota del estudiante";
                    $scope.modalInstanceError = $uibModal.open({
                        templateUrl: 'modules/modals/partials/modal-error.html',
                        scope: $scope,
                        size: 'sm',
                        keyboard  : false
                    });
                    vm.errorMessage = "Hubo un error al cargar la nota del estudiante"
                });
            vm.contador = 0;
            for (var i = 0; i < vm.estudiantes.length; i++) {
                if (vm.estudiantes[i].qualification != null) {
                    vm.contador++;
                }
            }
            if (vm.contador == vm.estudiantes.length && vm.evaluaciones.length > 1) {
                vm.notas = false;
                EvaluationCulminated.update({evaluationId: vm.evaluation.id}, null,
                    function (success) {
                    },
                    function (error) {
                        vm.message = "Believe or not person isn't at home, leave your message at the beep, it must be ot it would pick up the phone, where would it be? Believe or not person's not home..."
                    });
            }
        };

        vm.editarNota = function (calification) {
            calification.cambiar = false;
            calification.qualification.weight = vm.evaluation.weight;
            if (calification.id == vm.estudiante.qualificationId) {
                vm.estudiante.qualification = calification.qualification;
            }
            QualificationByStudentEdit.update({qualificationId: calification.id}, calification,
                function (success) {
                    vm.showSuccessMessageModal = true;
                    $timeout(function(){
                        vm.showSuccessMessageModal = false;
                    }, 2500);
                    vm.contador = 0;
                    for (var i = 0; i < vm.estudiantes.length; i++) {
                        if (vm.estudiantes[i].qualification != null) {
                            vm.contador++;
                        }
                    }
                    if (vm.contador == vm.estudiantes.length){
                        vm.notas = false;
                    }
                    vm.successMessageModal = "¡Nota guardada!";
                    vm.mensajeSuccessful = "¡nota guardada!";
                    $scope.modalInstanceSuccess = $uibModal.open({
                        templateUrl: 'modules/modals/partials/modal-succefull.html',
                        scope: $scope,
                        size: 'sm',
                        keyboard  : false
                    });
                },
                function (error) {
                    vm.showErrorMessageModalModal = true;
                    $timeout(function(){
                        vm.showErrorMessageModalModal = false;
                    }, 2500);
                    vm.mensajeError = "Hubo un error al editar la nota del estudiante";
                    $scope.modalInstanceError = $uibModal.open({
                        templateUrl: 'modules/modals/partials/modal-error.html',
                        scope: $scope,
                        size: 'sm',
                        keyboard  : false
                    });
                    vm.errorMessageModal = "Hubo un error al editar la nota del estudiante";
                });
        };

        vm.modificar = function (evaluacion) {
            if (evaluacion.cambiar) {
                evaluacion.cambiar = false;
            } else {
                evaluacion.cambiar = true;
            }
        };

        vm.cerrarl = function () {
            $scope.modalInstance.dismiss('cancel');
        };

        vm.siguienteEvaluacion = function () {
            vm.evaluation = vm.evaluaciones[0];
            vm.evaluaciones.splice(vm.evaluation, 1);
            if (vm.evaluaciones.length <= 1) {
                vm.siguiente = false;
            }
            CreateQualificationByMatterSectionByEvaluation.save({
                    matterId: vm.claseId,
                    evaluationId: vm.evaluation.id
                }, null,
                function (success) {
                    vm.notas = true;
                    vm.estudiantes = [];
                    vm.escala = vm.evaluation.evaluationScale.evaluationValueList;
                    for (var i = 0; i < success.response.qualificationUserList.length; i++) {
                        vm.estudiante = success.response.qualificationUserList[i].user;
                        vm.estudiante.qualificationId = success.response.qualificationUserList[i].id;
                        if (success.response.qualificationUserList[i].qualification.value != null) {
                            vm.estudiante.qualification = success.response.qualificationUserList[i].qualification;
                        } else {
                            vm.estudiante.qualification = null;
                        }
                        vm.estudiantes.push(vm.estudiante);
                    }
                },
                function (error) {
                    vm.message = "Believe or not person isn't at home, leave your message at the beep, it must be ot it would pick up the phone, where would it be? Believe or not person's not home..."
                });
        };

        //escucha cuando exista cambio en la comunidad seleccionada
        $scope.$on('community', function (evt, community) {
            vm.communitySelected = community;
            vm.messageInitial = "Por favor seleccione una comunidad";
            vm.loadInitial();
        });

        //Valida si el usuario esa autenticado con los permisos para la vista
        if (permission === null || authentication.getUser() === null) {
            $state.go('root');
            console.log("no esta autenticado:" + permission + " JSON:" + permission !== undefined ? JSON.stringify(permission) : "null");
        } else {
            // carga inicial por defecto
            vm.loadInitial();
        }

        vm.validarNota = function(Student){
            if(Student.qualification == null || Student.qualification == ""){
                Student.validate = true;
            }else{
                Student.validate = false;
            }
        };

        vm.cerrar = function(){
            $scope.modalInstanceSuccess.dismiss('cancel');
        };

        vm.salir = function(){
            $scope.modalInstanceError.dismiss('cancel');
        }
    }
})();
