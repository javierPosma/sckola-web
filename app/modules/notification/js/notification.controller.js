(function(){
    'use strict';
    angular.module('sckola')
        .controller('NotificationCtrl',NotificationCtrl);

    NotificationCtrl.$inject = ['$scope','$state','authentication', '$rootScope', 'NotificationInfoService', 'NotificationFactory', 'matchmedia', '$timeout', 'NotificationHistory', '$interval'];
    function NotificationCtrl($scope,$state,authentication, $rootScope, NotificationInfoService, NotificationFactory, matchmedia, $timeout, NotificationHistory, $interval){
        var vm = this;

        vm.user = authentication.getUser();
        var permission = authentication.currentUser();
        var idUser = authentication.getUser().id;
        $rootScope.tokenLoging = authentication.isLoggedIn();
        vm.activeCommunity = authentication.getCommunity();

        matchmedia.on("screen and (max-width: 991px)",function(){
            if(matchmedia.is("(max-width:991px)")){
                vm.slickSettings = {dots: true,
                    infinite: false,
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    method: {}
                }
            }else{
                vm.slickSettings = {dots: true,
                    infinite: false,
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    method: {}
                }
            }
        });

        vm.loadInitial = function(){
            NotificationInfoService.get(
                function(success){
                  vm.updateNotifications = false;
                  vm.notifications = success.response.notifications;
                  if(vm.activeCommunity != null){
                    NotificationFactory.get({communityId: vm.activeCommunity.communityOrigin.id},function(success){
                      vm.updateNotifications = false;
                      for(var a=0; a < success.response.length; a++) {
                        vm.notifications.push(success.response[a]);
                      }
                        vm.notificaciones = vm.chunk(vm.notifications,vm.chunkSize);
                      $timeout(function(){
                        vm.updateNotifications = true;
                      },1000);
                    },function(error){
                      vm.errorMessage = "No se han podido cargar las notificaciones";
                      vm.showErrorMessage();
                    });
                  }else{
                      vm.notificaciones = vm.chunk(vm.notifications,vm.chunkSize);
                    $timeout(function(){
                      vm.updateNotifications = true;
                    },1000);
                  }
                    //esto es para prueba
                    //for(var a=0; a < 4; a++) {
                    //    vm.notifications.push({});
                    //}
                    //vm.notificaciones = vm.chunk(vm.notifications,vm.chunkSize);
                },function(error){
                  vm.errorMessage = "No se han podido cargar las notificaciones";
                  vm.showErrorMessage();
                }
            );
        };
        vm.chunkSize = 4;
        var classMap = {
            5: 'notification-5',
            4: 'notification-4',
            3: 'notification-3',
            2: 'notification-2'
        };

        $scope.$on('change-chunk-size', function(event, data) {
            if (data !== vm.chunkSize) {
                vm.notificaciones = vm.chunk(vm.notifications, data);
                vm.chunkSize = data;
            }
        });

        vm.chunk = function(arr, size) {
            var newArr = [];
            var arrayLength = arr.length;
            for (var i = 0; i < arrayLength; i += size) {
                newArr.push(arr.slice(i, i + size));
            }
            return newArr;
        };

        $scope.getSlideClass = function(chunkSize) {
            if (classMap[chunkSize]) {
                return classMap[chunkSize];
            } else {
                return 'col-sm-10';
            }
        };

        vm.clickNotification = function(notification){
            if(notification.from == "Comunidad"){
                $state.go('communities');
            }
            if(notification.from == "Materia"){
                $state.go('matterHome');
            }
            if(notification.from == "Estudiantes"){
                $state.go("students");
            }
            if(notification.from == "Perfil"){
                $state.go("editProfile");
            }
            if(notification.from == "Plan de Evaluación"){
                $state.go("evaluationPlan");
            }
            if(notification.from == "Asistencia"){
                $state.go("assistances");
            }
            if(notification.from == "Calificaciones"){
                $state.go("uploadNotes");
            }
        };

        var promise = $interval(function()
            {
                NotificationInfoService.get(
                    function(success){
                        vm.updateNotifications = false;
                        vm.notifications = success.response.notifications;
                        if(vm.activeCommunity != null){
                            NotificationFactory.get({communityId: vm.activeCommunity.communityOrigin.id},function(success){
                                vm.updateNotifications = false;
                                for(var a=0; a < success.response.length; a++) {
                                    vm.notifications.push(success.response[a]);
                                }
                                vm.notificaciones = vm.chunk(vm.notifications,vm.chunkSize);
                                $timeout(function(){
                                    vm.updateNotifications = true;
                                },1000);
                            },function(error){
                                vm.errorMessage = "No se han podido cargar las notificaciones";
                                vm.showErrorMessage();
                            });
                        }else{
                            vm.notificaciones = vm.chunk(vm.notifications,vm.chunkSize);
                            $timeout(function(){
                                vm.updateNotifications = true;
                            },1000);
                        }
                        //esto es para prueba
                        //for(var a=0; a < 4; a++) {
                        //    vm.notifications.push({});
                        //}
                        //vm.notificaciones = vm.chunk(vm.notifications,vm.chunkSize);
                    },function(error){
                        vm.errorMessage = "No se han podido cargar las notificaciones";
                        vm.showErrorMessage();
                    }
                );
            },
            5000);

        $scope.$on('$destroy', function ()
        {
            $interval.cancel(promise);
        });


        //Valida si el usuario esa autenticado con los permisos para la vista
        if (permission === null || authentication.getUser() === null) {
            $state.go('root');
            console.log("no esta autenticado:" + permission + " JSON:" + permission!==undefined?JSON.stringify(permission):"null");
        }else{
            // carga inicial por defecto
            vm.loadInitial();
        }


    }


})();
