(function(){
    'use strict';

    angular
        .module('sckola.login')
        .factory('Login', Login)
        .factory('LoginFacebook', LoginFacebook)
        .factory('RecoverPassword', recoverPassword)
        .factory('CreateUserFacebook', CreateUserFacebook)
        .factory('FacebookService', facebookService);

    Login.$inject = ['$resource','$rootScope'];
    function Login($resource, $rootScope){
        return $resource($rootScope.domainUrl+'/skola/login');
    };

    LoginFacebook.$inject = ['$resource','$rootScope'];
    function LoginFacebook($resource, $rootScope){
        return $resource($rootScope.domainUrl+'/skola/login/facebook');
    };

    recoverPassword.$inject = ['$resource','$rootScope'];
    function recoverPassword($resource,$rootScope){
        return $resource($rootScope.domainUrl + '/skola/user/recover',{ mail: '@mail' });
    };

    CreateUserFacebook.$inject = ['$resource','$rootScope'];
    function CreateUserFacebook($resource, $rootScope){
        return $resource($rootScope.domainUrl+'/skola/user/facebook',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                }
            });
    };

    facebookService.$inject = [];
    function facebookService($q) {
        return {
            getMyLastName: function() {
                var deferred = $q.defer();
                FB.api('/me', {
                    fields: 'last_name'
                }, function(response) {
                    if (!response || response.error) {
                        deferred.reject('Error occured');
                    } else {
                        deferred.resolve(response);
                    }
                });
                return deferred.promise;
            }
        }
    };



})();
