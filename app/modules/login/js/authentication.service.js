(function(){
    'use strict';

    angular
        .module('sckola.login')
        .service('authentication', authentication);

    authentication.$inject = ['$http', '$window'];
    function authentication ($http, $window) {

        var saveToken = function (token) {
            $window.localStorage.setItem('skola-token',token);
        };

        var getToken = function () {
            return $window.localStorage.getItem('skola-token');
        };

        var saveUser = function (user) {
            if(user !== undefined)
                $window.localStorage.setItem("skola-user",JSON.stringify(user));
            else
                $window.localStorage.setItem("skola-user",undefined);
        };

        var getUser = function () {
            var aux = $window.localStorage.getItem("skola-user");
            if(aux !== "undefined" && aux !== null)
                return JSON.parse($window.localStorage.getItem("skola-user"));
            else
                return undefined;
        };

        var setCommunity = function (community) {
            //console.log(typeof community)
            if(community !== undefined)
                $window.localStorage.setItem("skola-currentCommunity",JSON.stringify(community));
            else
                $window.localStorage.setItem("skola-currentCommunity",undefined);
        };

        var getCommunity = function () {
            //console.log($window.localStorage.getItem("skola-currentCommunity"))
            var aux = $window.localStorage.getItem("skola-currentCommunity");
            //console.log(typeof aux)
            if(aux !== "undefined" && aux !== null)
                return  JSON.parse($window.localStorage.getItem("skola-currentCommunity"));
            else
                return undefined;
        };

        var getWizardState = function(){

            var aux = $window.localStorage.getItem("skola-currentwizardStatus");

            if(aux !== "undefined" && aux !== null)
                return $window.localStorage.getItem("skola-currentwizardStatus");
            else
                return undefined;
        };

        var setWizardState = function(wizardState){

            if(wizardState !== undefined)
                $window.localStorage.setItem("skola-currentwizardStatus", wizardState);
            else
                $window.localStorage.setItem("skola-currentwizardStatus", undefined);
        };
/*
        var saveRoleCode = function (token) {
            $window.localStorage['code-token'] = token;
        };

        var getRoleCode = function () {
            return $window.localStorage['code-token'];
        };

        var saveRoleId = function (token) {
            $window.localStorage['id-token'] = token;
        };

        var getRoleId = function () {
            return $window.localStorage['id-token'];
        };
        */

        var logout = function() {
            $window.localStorage.clear();
            /*
            $window.localStorage.removeItem('skola-token');
            $window.localStorage.removeItem('skola-user');
            $window.localStorage.removeItem('skola-currentCommunity');
            */
        };

        var isTokenValid = function() {
            var token = getToken();
            var payload = token.split('.')[1];
            payload = $window.atob(payload);
            payload = JSON.parse(payload);
            return payload.exp;
        };

        /*

         {
         "role":[{"authority":"TEACHER"},{"authority":"STUDENT"}],
         "created":1495580044660,
         "_id":"POSMA@GMAIL.COM",
         "exp":1495598044}
         */
        var isLoggedIn = function() {
            var token = getToken();
            var payload;

            if(token){
                payload = token.split('.')[1];
                payload = $window.atob(payload);
                payload = JSON.parse(payload);

                return payload.exp > Date.now() / 1000;
            } else {
                return false;
            }
        };

        var currentUser = function() {
            if(isLoggedIn()){
                var token = getToken();
                var payload = token.split('.')[1];
                payload = $window.atob(payload);
                payload = JSON.parse(payload);

                return {
                    role: payload.role[0].authority,
                    _id:payload._id,
                    status: payload.status,
                    identification: payload.identification
                };
            } else {
                //return;
                return getUser()!==undefined?getUser():null;
            }
        };

        return {
            saveToken : saveToken,
            getToken : getToken,
            logout : logout,
            isLoggedIn : isLoggedIn,
            isTokenValid : isTokenValid,
            currentUser : currentUser,
            //getRoleCode : getRoleCode,
            //saveRoleCode : saveRoleCode,
            //getRoleId : getRoleId,
            //saveRoleId : saveRoleId,
            getUser : getUser,
            saveUser : saveUser,
            getCommunity : getCommunity,
            setCommunity : setCommunity,
            getWizardState : getWizardState,
            setWizardState : setWizardState
        };
    }
})();
