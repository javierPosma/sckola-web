
/**
* Controllers
* @namespace Controllers
*/
(function(){
  'use strict';

  angular.module('sckola.dashboard')
  .controller('FeedCtrl', FeedCtrl);

  FeedCtrl.$inject = ['$scope','$timeout','$rootScope','authentication','NotificationService',
  'Notification','NotificationFactory','RemoveNotification','StudentsBySection','AssistanceStudents',
  'CreateQualificationByMatterSectionByEvaluation','QualificationByStudentEdit',
  'EvaluationCulminated','matchmedia','NotificationInfoService','$state'];
  /**
  * @namespace FeedController
  * @desc Controller for dashboard feed, handle notifications of assistance and scores.
  * @memberOf Controllers
  */
  function FeedCtrl($scope,$timeout,$rootScope,authentication,NotificationService,
    Notification,NotificationFactory,RemoveNotification,StudentsBySection,AssistanceStudents,
    CreateQualificationByMatterSectionByEvaluation,QualificationByStudentEdit,
    EvaluationCulminated,matchmedia,NotificationInfoService,$state){
      var vm = this;
      var permission = authentication.currentUser();
      /*
      * Notification types:
      * "ASSISTANCE" ------> Assistance
      * "SCORE" ------> Scores
      * "INFO" ------> Info
      */
      vm.activeCommunity = authentication.getCommunity();
      vm.errorGettingAssistance = "Oops! Ha ocurrido un error obteniendo la lista de estudiantes!";
      vm.showError = false;
      vm.errorMessage = "¡Oops! Algo ha salido mal";
      vm.notifications = [];
      vm.students = [];
      vm.scoreStudents = [];
      vm.slides = [];
      vm.showAssistance = false;
      vm.showCarousel = true;
      vm.showScore = false;
      vm.successMessage = "";
      vm.updateNotifications = true;
      vm.scores = [];
    var finalDate = null;
      $rootScope.tokenLoging = authentication.isLoggedIn();

      matchmedia.on("screen and (max-width: 991px)",function(){
        if(matchmedia.is("(max-width:991px)")){
          vm.slickSettings = {
            infinite:false,
            slidesToShow:3,
            slidesToScroll:3,
            dots:true,
            arrows:false,
            method: {}
          }
        }else{
          vm.slickSettings = {
            infinite:false,
            slidesToShow:3,
            slidesToScroll:3,
            dots:true,
            method: {}
          }
        }
      });


      /**
      * This event alerts incoming notification from the websocket.
      */
      //NotificationService.receive().then(null, null, function(message) {
//
      //  if(authentication.getCommunity()!=null && message.community.id == authentication.getCommunity().communityOrigin.id){
      //    vm.updateNotifications = false;
      //    Notification.initialize('/static/images/usuario.png');
      //    Notification.notify(message.text,message.from);
      //    vm.notifications.push(message);
      //    $timeout(function(){
      //      vm.updateNotifications = true;
      //    },1000);
      //  }
//
      //});


      /**
      * @name showErrorMessage
      * @desc Shows error message to the user
      * @memberOf Controllers.FeedController
      */
      vm.showErrorMessage = function(){
        vm.showError = true;
        $timeout(function(){
          vm.showError = false;
        },2500);
      };

      /**
      * @name loadInitial
      * @desc Load the initial data for the dashboard, showing all active notifications in the active community for the user
      * @memberOf Controllers.FeedControler
      */
      vm.loadInitial = function(){

        vm.notifications = [];
        vm.showAssistance = false;
        vm.showScore = false;
        //NotificationInfoService.get(
        //    function(success){
        //      vm.updateNotifications = false;
        //      vm.notifications = success.response.notifications;
        //      if(vm.activeCommunity != null){
        //        NotificationFactory.get({communityId: vm.activeCommunity.communityOrigin.id},function(success){
        //          vm.updateNotifications = false;
        //          for(var a=0; a < success.response.length; a++) {
        //            vm.notifications.push(success.response[a]);
        //          }
        //          $timeout(function(){
        //            vm.updateNotifications = true;
        //          },1000);
        //        },function(error){
        //          vm.errorMessage = "No se han podido cargar las notificaciones";
        //          vm.showErrorMessage();
        //        });
        //      }else{
        //        $timeout(function(){
        //          vm.updateNotifications = true;
        //        },1000);
        //      }
        //    },function(error){
        //      vm.errorMessage = "No se han podido cargar las notificaciones";
        //      vm.showErrorMessage();
        //    }
        //);

      };

      /**
      * @name setImagesInStudents
      * @desc Set a ghostPerson.png as image for the students
      * @memberOf Controllers.FeedControler
      */
      vm.setImagesInStudents = function(){

        for(var i=0; i<vm.students.length; i++){
          vm.students[i].assistance = true;
          vm.students[i].image = "static/images/ghostPerson.png";
        }

      };

      /**
      * @name getDate
      * @desc gets a String date to send to the API with the correct format
      * @param {string} date Date with different format
      * @memberOf Controllers.FeedControler
      */
      vm.getDate = function(date){

        var notificationDate = date.split(' ')[0];
        var assistanceDate = notificationDate.split('-');
        finalDate = assistanceDate[2] + "-" + assistanceDate[1] + "-" + assistanceDate[0];
        return finalDate;

      };


      /**
      * @name getAssistanceList
      * @desc Gets all the students that belong to the section.
      * @param {int} sectionId Id of the matter section
      * @memberOf Controllers.FeedControler
      */
      vm.getAssistanceList = function(sectionId){

        StudentsBySection.get({id: sectionId},function(success){
          vm.students = success.response;
          vm.setImagesInStudents();
        },function(error){
          vm.errorMessage = "No se ha podido cargar la asistencia";
          vm.showErrorMessage();
        });

      };

      /**
      * @name setScoreList
      * @desc Set the evaluation scale to display in the view.
      * @param {object} evaluation Evaluation that contains the scale
      * @memberOf Controllers.FeedControler
      */
      vm.setScoreList = function(evaluation){

        vm.scores = [];
        var listScale = evaluation.evaluationScale.evaluationValueList;
        for(var point in listScale){
          vm.scores.push(point);
        }

      };

      /**
      * @name setScoreStudentList
      * @desc Set the students to evaluate for display in the dashboard, depending on the evaluation status.
      *       if the status is 'PENDING' then with the students to evaluate we create the empty notes of the evaluation for each student.
      *       else it means that the empty notes were already created so we just set the list to display.
      * @param {object} notification Notification that has the evaluation to execute in the dashboard, it contains the students with their notes.
      * @memberOf Controllers.FeedControler
      */
      vm.setScoreStudentList = function(notification){

        vm.scoreStudents = [];
        vm.activeEvaluation = notification.evaluation;
        vm.scores = notification.evaluation.evaluationScale.evaluationValueList;
        if(notification.evaluation.status=="PENDING"){
          CreateQualificationByMatterSectionByEvaluation.save({
            matterId: notification.matter.id,
            evaluationId: notification.evaluation.id
          }, null,
          function (success) {
            if(success.message != "Students no exists"){
              for (var i = 0; i < success.response.qualificationUserList.length; i++) {
                var student = success.response.qualificationUserList[i].user;
                student.qualificationId = success.response.qualificationUserList[i].id;
                student.image = "static/images/ghostPerson.png";
                if (success.response.qualificationUserList[i].qualification != null) {
                  student.qualification = success.response.qualificationUserList[i].qualification;
                } else {
                  student.qualification = null;
                }
                vm.scoreStudents.push(student);
              }
              $timeout(function(){
                vm.terminate = true;
              }, 2500);
            }else{
              $timeout(function(){
                vm.terminate = true;
              }, 2500);
              vm.show = false;
              vm.messageClaseShow = true;
              vm.messageClase = "No existen estudiantes en la clase"
            }
          },
          function (error) {
            vm.showErrorMessage();
          });
        }
        else{
          vm.contador = 0;
          for (var i = 0; i < notification.evaluation.qualificationUserList.length; i++) {
            var student = notification.evaluation.qualificationUserList[i].user;
            student.qualificationId = notification.evaluation.qualificationUserList[i].id;
            student.qualification = notification.evaluation.qualificationUserList[i].qualification;
            student.image = "static/images/ghostPerson.png";
            if (student.qualification != null) {
              vm.contador++;
            }
            vm.scoreStudents.push(student);
          }
        }

      };


      /**
      * @name showNotificationInfo
      * @desc show information in dashboard depending of the notification
      * @param {int} notificationType Notification type
      * @param {int} notificationId Notification id
      * @memberOf Controllers.FeedControler
      */
      vm.showNotificationInfo = function(notification){

        vm.activeNotification = notification;
        vm.inactivateNotification(notification.id);
        switch (notification.type){
          case "ASSISTANCE":
          vm.dates = vm.getDate(notification.date);
          vm.getAssistanceList(notification.section.id);
          if(vm.showScore){
            vm.showScore = false;
            $timeout(function () {
              vm.showAssistance = true
            }, 500);
          }
          else{
            vm.showAssistance = true;
          }
          break;
          case "SCORE":
          vm.getAssistanceList(notification.section.id);
          vm.setScoreList(notification.evaluation);
          vm.setScoreStudentList(notification);
          if(vm.showAssistance){
            vm.showAssistance = false;
            $timeout(function(){
              vm.showScore = true;
            }, 500);
          }
          else{
            vm.showScore = true;
          }
          break;
          case "INFO":
            $state.go("editProfile");
            break;
        }

      };

      /**
      * @name inactivateNotification
      * @desc removes flag of the carousel notification by id
      * @param {int} notificationType Notification id
      * @memberOf Controllers.FeedControler
      */
      vm.inactivateNotification = function(id){

        for(var i=0; i < vm.notifications.length ; i ++){
          if(vm.notifications[i].id === id){
            vm.notifications[i].active = false;
          }
        }

      };

      /**
      * @name saveScore
      * @desc Save score in system, show sucess message
      *       and removes notification from carousel
      * @memberOf Controllers.FeedControler
      */
      vm.saveScore = function(){

        EvaluationCulminated.update({evaluationId: vm.activeEvaluation.id}, null,
          function (success) {
            vm.showScore = false;
            vm.successMessage = "¡Evaluación culminada!";
            vm.showSuccessMessage = true;
            vm.removeNotification(vm.activeNotification.id);
            $timeout(function () {
              vm.showSuccessMessage = false;
            }, 2500);
          },
          function (error) {
            vm.message = "Believe or not person isn't at home, leave your message at the beep, it must be ot it would pick up the phone, where would it be? Believe or not person's not home..."
            vm.showErrorMessage();
          });

        };

        /**
        * @name uploadNote
        * @desc Upload the qualification of a student for an evaluation
        * @param {object} student Student to asociate qualification.
        * @memberOf Controllers.FeedControler
        */
        vm.uploadNote = function(student){

          var studentNote = {"qualification": student.qualification};
          studentNote.qualification.weight = vm.activeEvaluation.weight;
          QualificationByStudentEdit.update({qualificationId: student.qualificationId}, studentNote,
            function (success) {
              vm.successMessage = "¡Nota guardada exitosamente!";
              vm.showSuccessMessage = true;
              $timeout(function () {
                vm.showSuccessMessage = false;
              }, 2500);
            },
            function (error) {
              vm.showErrorMessage();
            });
            vm.contador = 0;
            for (var i = 0; i < vm.scoreStudents.length; i++) {
              if (vm.scoreStudents[i].qualification != null) {
                vm.contador++;
              }
            }
            if (vm.contador == vm.estudiantes.length && vm.evaluaciones.length >= 1) {
              vm.notas = false;
            }

          };

          /**
          * @name saveAssistance
          * @desc save assistance in system, show sucess message
          *       and removes notification from carousel
          * @memberOf Controllers.FeedControler
          */
          vm.saveAssistance = function(){

            vm.assistence = {"date": null, "matterCommunitySectionId": null, "userAssistList": []};
            vm.assistence.matterCommunitySectionId = vm.activeNotification.matter.id;
            var date = vm.activeNotification.date.split(" ")[0];
            var dividedDate = date.split("-");
            vm.assistence.date = dividedDate[2] +"-"+dividedDate[1] +"-"+dividedDate[0];
            for (var i = 0; i < vm.students.length; i++) {
              var student = {"userId": vm.students[i].id, "assistance": vm.students[i].assistance};
              vm.assistence.userAssistList.push(student);
            }
            AssistanceStudents.save({matterCommunityId: vm.activeNotification.matter.id}, vm.assistence,
              function (success) {
                vm.edit = true;
                vm.showAssistance = false;
                vm.showSuccessMessage = true;
                vm.showSuccessAssistance = true;
                vm.successMessage = "¡Asistencia guardada exitosamente!";
                vm.removeNotification(vm.activeNotification.id);
                $timeout(function(){
                  vm.showSuccessMessage = false;
                }, 2500);
              },
              function (error) {
                vm.showErrorMessage();
              })

            };

            /**
            * @name removeNotification
            * @desc removes notification only from carousel using activeSlick property,
            * it does not remove the notification from the vm.news array because it causes
            * an strange behaviour with angular-slick-carousel.
            * @param {int} id id of the notification.
            * @memberOf Controllers.FeedControler
            */
            vm.removeNotification = function(id){

              vm.updateNotifications = false;
              var slickPosition = 0;
              for(var i=0; i < vm.notifications.length ; i ++){
                if(vm.notifications[i].id === id){
                  vm.slickSettings.method.slickRemove(slickPosition);
                  vm.notifications[i].activeSlick = false;
                  vm.notifications.splice(i,1);
                }
                slickPosition = slickPosition +1;
              }
              RemoveNotification.update({notificationId:id},function(success){
              },function(error){
              vm.showErrorMessage();
              });
              $timeout(function(){
                vm.updateNotifications = true;
              },1000);

            };

            //Event gets triggered when a change of community is detected
            $scope.$on('community', function (evt, community) {
                vm.activeCommunity = community;
                vm.messageInitial = "Por favor seleccione una comunidad";
                vm.loadInitial();
            });

            //Validates if the user is authenticated or not.
            if (permission === null || authentication.getUser() === null) {
                $state.go('root');
                console.log("no esta autenticado:" + permission + " JSON:" + permission !== undefined ? JSON.stringify(permission) : "null");
            } else {
                //Loads initial data of the controller
                vm.loadInitial();
            }

          }

        })();
