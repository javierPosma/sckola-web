/**
* Services
* @namespace Services
*/
angular.module("sckola.dashboard")
.factory("NotificationFactory",NotificationFactory)
.factory("RemoveNotification",RemoveNotification)
.factory("NotificationHistory",NotificationHistory)
.service("NotificationService", NotificationService )
    .factory("NotificationInfoService", NotificationInfoService);


RemoveNotification.$inject = ['$resource','$rootScope'];
/**
*
* @name RemoveNotification
* @desc Factory to set an active notification to inactive
* @param {object} $resource - resource
* @param {object} $rootScope - rootScope
* @memberOf Services.FeedService
*/
function RemoveNotification($resource,$rootScope){
  return $resource($rootScope.domainUrl+'skola/notification/remove/:notificationId',{notificationId: '@notificationId'},
  {
    update: {
      method: 'PUT'
      /*headers: {
      Authorization: 'Bearer '+ authentication.getToken()
    }*/
  }
});
}


NotificationHistory.$inject = ['$resource','$rootScope','authentication'];
function NotificationHistory($resource,$rootScope,authentication){
  return $resource($rootScope.domainUrl+'skola/notification/history/:communityId?email='+authentication.getUser(),{communityId: '@communityId'},
  {
    get: {
      method: 'GET'
    }
  });
}

NotificationFactory.$inject = ['$resource','$rootScope','authentication'];
/**
*
* @name NotificationFactory
* @desc Factory to get all the notifications of an user by community.
* @param {object} $resource - resource
* @param {object} $rootScope - rootScope
* @param {object} authentication - authentication service to get the user.
* @memberOf Services.FeedService
*/
function NotificationFactory($resource,$rootScope, authentication){
  return $resource($rootScope.domainUrl+'skola/notification/:communityId?email='+authentication.getUser().mail,{communityId: '@communityId'},
  {

    get: {
      method: 'GET'
    } ,
    query: {
      method: 'GET',
      isArray:true
    }
  });
}

function NotificationInfoService($resource,$rootScope, authentication){
  return $resource($rootScope.domainUrl+'skola/notification/info?email='+authentication.getUser().mail,
      {

        get: {
          method: 'GET'
        } ,
        query: {
          method: 'GET',
          isArray:true
        }
      });
}

NotificationService.$inject = ['$q', '$timeout', 'authentication','$rootScope'];
/**
*
* @name NotificationService
* @desc Service that configures the websocket.
* @param {object} $q - q
* @param {object} $timeout - timeout
* @param {object} authentication - authentication service to get the user.
* @memberOf Services.FeedService
*/
function NotificationService ($q, $timeout, authentication, $rootScope) {

  var service = {}, listener = $q.defer(), socket = {
    client: null,
    stomp: null
  }, messageIds = [];

  var user = authentication.getUser();
  service.RECONNECT_TIMEOUT = 30000;
  service.SOCKET_URL = $rootScope.domainUrl+"notification";
  service.CHAT_TOPIC = "/dashboard/message";
  service.CHAT_BROKER = "/app/notification";

  /**
  * @name service.receive
  * @desc return listener promise
  * @memberOf Services.FeedService
  */
  service.receive = function() {
    return listener.promise;
  };

  /**
  * @name service.send
  * @param {object} message - Message to send via websocket.
  * @desc Sends a  message to the web service via websocket.
  * @memberOf Services.FeedService
  */
  service.send = function(message) {
    var id = Math.floor(Math.random() * 1000000);
    socket.stomp.send(service.CHAT_BROKER, {
      priority: 9
    }, JSON.stringify({
      message: message,
      id: id
    }));
    messageIds.push(id);
  };

  var reconnect = function() {
    $timeout(function() {
      initialize();
    }, this.RECONNECT_TIMEOUT);
  };

  /**
  * @name getMessage
  * @param {object} data - Incoming notification from the web services.
  * @desc Gets the notification message and assigns active and activeSlick attributes to true.
  * @memberOf Services.FeedService
  */
  var getMessage = function(data) {
    var notification = JSON.parse(data)
    notification.active = true;
    notification.activeSlick = true;
    return notification;
  };

  /**
  * @name startListener
  * @desc Starts the listener for notifications.
  * @memberOf Services.FeedService
  */
  var startListener = function() {

    socket.stomp.subscribe('/user/' + user.mail + '/notify',function(data){
      listener.notify(getMessage(data.body));
    });

  };

  /**
  * @name initialize
  * @desc Initialize the websocket.
  * @memberOf Services.FeedService
  */
  var initialize = function() {

    socket.client = new SockJS($rootScope.domainUrl+'stomp');
    socket.stomp = Stomp.over(socket.client);
    socket.stomp.connect("posma","", startListener);
    socket.stomp.onclose = reconnect;

  };

  initialize();
  return service;
}
