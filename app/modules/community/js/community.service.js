/**
 * Factories
 * @namespace Factories
 */
(function(){

  'use strict';

  angular.module('sckola.community')
         .factory('Communities',Communities)
         .factory('ComunnityAssociate', CommunityAssociate)
         .factory('ComunidadMaterias',ComunidadMaterias)
         .factory('CommunityUser',CommunityUser);


         //TODAS LAS COMUNIDADES
         Communities.$inject = ['$resource','$rootScope'];
         /**
          * @namespace ComunitiesFactory
          * @desc Gets all the comminities
          * @memberOf Factories
          */
         function Communities($resource,$rootScope){
             //return $resource('static/data/comunidades.json');
             return $resource($rootScope.domainUrl+'/skola/community',null,
                 {
                     save: {
                         method: 'POST'
                         /*headers: {
                          Authorization: 'Bearer '+ authentication.getToken()
                          }*/
                     },
                     get: {
                         method: 'GET'
                     } ,
                     query: {
                         method: 'GET',
                         isArray:true
                     }
                 });
         };

         //SERVICIO PARA ASOCIAR LA COMUNIDAD A UN USUARIO
         CommunityAssociate.$inject = ['$resource','$rootScope'];
         /**
          * @namespace CommunityAssociateFactory
          * @desc Associates a community to a teacher
          * @memberOf Factories
          */
         function CommunityAssociate($resource,$rootScope){
             return $resource($rootScope.domainUrl+'/skola/communityUser/community/:communityId/user/:userId/role/:roleId',null,
                 {
                     save: {
                         method: 'POST'
                         /*headers: {
                             Authorization: 'Bearer '+ authentication.getToken()
                         }*/
                     },
                     get: {
                         method: 'GET'
                     } ,
                     query: {
                         method: 'GET',
                         isArray:true
                     }
                 });
         };

         //COMUNIDADES DE UN USUARIO Y ROL
         CommunityUser.$inject = ['$resource','$rootScope'];
         /**
          * @namespace CommunityUser
          * @desc Gets all comunities from an user and role
          * @memberOf Factories
          */
         function CommunityUser($resource,$rootScope){
             return $resource($rootScope.domainUrl+'/skola/communityUser/user/:userId/role/:roleId');
         };

         //OBTIENE TODAS LAS MATERIAS EXISTENTES PARA UNA COMUNIDAD USUARIO ROL
         ComunidadMaterias.$inject = ['$resource','$rootScope'];
         /**
          * @namespace ComunidadMateriasFactory
          * @desc Get alls subjects from a community
          * @memberOf Factories
          */
         function ComunidadMaterias($resource,$rootScope){
             return $resource($rootScope.domainUrl+'/skola/matter/community/:communityId/role/:roleId/user/:userId');
             //return $resource('static/data/communityMatters.json');
         };

})();
