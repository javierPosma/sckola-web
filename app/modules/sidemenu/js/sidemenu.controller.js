(function(){
  'use strict';
  angular.module('sckola')
         .controller('SideMenuCtrl',SideMenuCtrl);

  SideMenuCtrl.$inject = ['$scope','$state','authentication', '$rootScope', 'UserUpdate', 'UserPhoto'];
  function SideMenuCtrl($scope,$state,authentication, $rootScope, UserUpdate, UserPhoto){
    var vm = this;

    vm.goToCommunities = function(){
      $state.go('communities');
    };

    vm.goToCourses = function(){
      $state.go('matterHome');
    };

    vm.goToAssistances = function(){
      $state.go("assistances");
    };

    vm.goToStudents = function(){
      $state.go("students");
    };

    vm.gotToEvaluations = function(){
      $state.go("evaluationPlan");
    };

    vm.goToUploadNotes = function(){
      $state.go("uploadNotes");
    };

    /**
    * @name goToProfile
    * @desc Changes state to profile.
    * @memberOf Controllers.SideMenuController
    */
    vm.goToProfile = function(){
      $state.go("editProfile");
    };


      vm.showMyCommunities = false;

        vm.user = authentication.getUser();
        var permission = authentication.currentUser();
        var idUser = vm.user.id;
        $rootScope.tokenLoging = authentication.isLoggedIn();

        vm.loadInitial = function(){
            UserUpdate.get({userId: idUser},
                function(success){
                    vm.usuario=success.response;

                    if(vm.usuario.firstName == null || vm.usuario.firstName == ""){
                          vm.usuario.firstName = "Configurar en";
                          vm.usuario.lastName = "Perfil ";
                    }

                    if(vm.usuario.foto == null){
                          vm.usuario.foto = "static/images/usuario.png";
                    }else if (!vm.usuario.foto.includes('https')){
                        vm.usuario.foto = "static/images/" + vm.usuario.foto;
                    }
                    for(var i=0;i<vm.usuario.curriculum.length;i++){
                        if(vm.usuario.curriculum[i].date != null){
                            var formatoDate = vm.usuario.curriculum[i].date.split("-");
                            vm.usuario.curriculum[i].anio = Number(formatoDate[2]);
                        }
                    }
                    if(vm.usuario.birthdate != null){
                        var formato = vm.usuario.birthdate.split("-");
                        vm.usuario.birthdateOld = new Date(formato[2],formato[1]-1,formato[0]);
                    }
                },
                function(error){

                });
        };

        //Valida si el usuario esa autenticado con los permisos para la vista
        if (permission === null || authentication.getUser() === null) {
            $state.go('root');
            console.log("no esta autenticado:" + permission + " JSON:" + permission!==undefined?JSON.stringify(permission):"null");
        }else{
            // carga inicial por defecto
            vm.loadInitial();
        }



        $scope.foto = function(imagen){
           vm.imagen = new FormData();
            vm.imagen.append("file", imagen[0]);
            UserPhoto.save({userId:idUser},vm.imagen,
            function(success){
                UserUpdate.update({userId:idUser},success.response,
                function(success){
                    vm.usuario=success.response;
                    $scope.$parent.$broadcast('userSave',success.response);
                    if(vm.usuario.birthdate != null){
                        var formato = vm.usuario.birthdate.split("-");
                        vm.usuario.birthdateOld = new Date(formato[2],formato[1]-1,formato[0]);
                    }
                    $scope.$parent.$broadcast('userUpdate',success.response);
                });
            },
            function(error){
            });
       };

  }


})();
