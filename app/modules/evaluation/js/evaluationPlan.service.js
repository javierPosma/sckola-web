/**
 * Created by Posma-ricardo on 03/08/2017.
 */
 /**
  * Factories
  * @namespace Factories
  */
(function(){
    'use strict';

    angular
        .module('sckola.evaluationPlan')
        .factory('EvaluationPlan', evaluationPlan)
        .factory('EvaluationPlanTemplate',evaluationPlanTemplate)
        .factory('EvaluationTools',evaluationTools)
        .factory('EvaluationPlanFromTeacher', evaluationPlanFromTeacher)
        .factory('EvaluationPlanDetail', evaluationPlanDetail)
        .factory('CreateEvaluationPlan', createEvaluationPlan)
        .factory('AssociatePlanEvaluationClass', associatePlanEvaluationClass)
        .factory('UpdateEvaluation', updateEvaluation)
        .factory('CreateEvaluation', createEvaluation)
        .factory('AssociateEvaluationPlanMatter',associateEvaluationPlanMatter)
        .factory('EvaluationPlanCopy', evaluationPlanCopy)
        .factory('Scales',scales)
        .value('data', {});

    evaluationPlan.$inject = ['$resource','$rootScope'];
    /**
     * @namespace evaluationPlan
     * @desc Evaluation Plan Json File
     * @memberOf Factories
     */
    function evaluationPlan($resource,$rootScope){
        return $resource('static/data/evaluationPlan.json');
    };

    //GET DE LAS PLANTILLAS DE EVALUACION
    evaluationPlanTemplate.$inject = ['$resource','$rootScope'];
    /**
     * @namespace evaluationPlanTemplate
     * @desc Gets all the evaluation plan templates
     * @memberOf Factories
     */
    function evaluationPlanTemplate($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/template/evaluation/plan');
        //MCreturn $resource($rootScope.domainUrl+'/skola/template/evaluation/plan');
    };

    evaluationPlanCopy.$inject = ['$resource','$rootScope'];
    /**
     * @namespace evaluationPlanCopy
     * @desc Copy an evaluation plan
     * @memberOf Factories
     */
    function evaluationPlanCopy($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/plan/template/:planIdToCopy',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                }
            });
        //MCreturn $resource($rootScope.domainUrl+'/skola/template/evaluation/plan');
    };

    //GET DE LAS HERRAMIENTAS DE EVALUACION
    evaluationTools.$inject = ['$resource','$rootScope'];
    /**
     * @namespace evaluationTools
     * @desc Get all the evaluation tools
     * @memberOf Factories
     */
    function evaluationTools($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/tool');
    };

    //GET de las evaluacion plan del profesor de una comunidad que esten asociados y que no esten asociados a una clase
    evaluationPlanFromTeacher.$inject = ['$resource','$rootScope'];
    /**
     * @namespace evaluationPlanFromTeacher
     * @desc Get all evaluation plan of a teacher
     * @memberOf Factories
     */
    function evaluationPlanFromTeacher($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/plan/user/:userId/community/:communityId');
    };

    //GET del detalle del plan de evaluacion
    evaluationPlanDetail.$inject = ['$resource','$rootScope'];
    /**
     * @namespace evaluationPlanDetail
     * @desc Gets detail of one evaluation plan by id
     * @memberOf Factories
     */
    function evaluationPlanDetail($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/plan/:planId');
    };

    //Crea un nuevo plan de evaluacion
    createEvaluationPlan.$inject = ['$resource','$rootScope'];
    /**
     * @namespace createEvaluationPlan
     * @desc Creates an evaluation plan
     * @memberOf Factories
     */
    function createEvaluationPlan($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/plan/:planId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                update:{
                    method: 'PUT'
                }
            });
    };

    //Crea una evaluacion para el plan de evaluacion
    createEvaluation.$inject = ['$resource','$rootScope'];
    /**
     * @namespace createEvaluation
     * @desc Creates an evaluation for an evaluation plan
     * @memberOf Factories
     */
    function createEvaluation($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/evaluation_plan/:planId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                }
            });
    };

    //Associa un plan de evaluacion a una clase
    associatePlanEvaluationClass.$inject = ['$resource','$rootScope'];
    /**
     * @namespace associatePlanEvaluationClass
     * @desc Associates the evaluation plan to one class
     * @memberOf Factories
     */
    function associatePlanEvaluationClass($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/plan/:planId/matter_community_section/:matterCommunitySectionId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                }
            });
    };

    //Actualiza una evaluacion de un plan de evaluacion
    updateEvaluation.$inject = ['$resource','$rootScope'];
    /**
     * @namespace updateEvaluation
     * @desc Updates an evaluation of a evaluation plan
     * @memberOf Factories
     */
    function updateEvaluation($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/:evaluationId',null,
            {
                update: {
                    method: 'PUT'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                delete: {
                    method: 'DELETE'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                }
            });
    };

    //SERVICIO PARA ASOCIAR UN PLAN DE EVALUACION A UNA MATERIA
    associateEvaluationPlanMatter.$inject = ['$resource','$rootScope'];
    /**
     * @namespace associateEvaluationPlanMatter
     * @desc Associates the evaluation plan to one matter
     * @memberOf Factories
     */
    function associateEvaluationPlanMatter($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/plan/:planId/matter_community_section/:matterCommunitySectionId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                }
            });
    };

    //SERVICIO PARA OBTENER LAS ESCALAS EXISTENTES
    scales.$inject = ['$resource','$rootScope'];
    /**
     * @namespace scales
     * @desc Gets all the existing scales
     * @memberOf Factories
     */
    function scales($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/scale',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                }
            });
    };



})();
