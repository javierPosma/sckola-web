(function(){
  'use strict';
  var actualSection = "section1";
  angular.module('sckola')
          .directive('setScreenHeight', function($window){
            return {
              link: function(scope,element,attrs){
                switch(element[0].id){
                  case "section1":
                    element.css('height', $window.innerHeight-107 + 'px');
                    break;
                  case "section2":
                    element.css('min-height', $window.innerHeight + 'px');
                    break;
                  case "section3":
                    element.css('height', $window.innerHeight -23 + 'px');
                    break;
                  default :
                    element.css('min-height', $window.innerHeight + 'px');
                    break;
                }

              }
            }
          })
          .directive('setMinScreenHeight', function($window){
            return {
              link: function(scope,element,attrs){
                switch(element[0].id){
                  case "section1":
                    element.css('min-height', $window.innerHeight-107 + 'px')
                    break;
                  case "section2":
                    element.css('min-height', $window.innerHeight + 'px');
                    break;
                  case "section3":
                    element.css('min-height', $window.innerHeight -23 + 'px');
                    break;
                  default :
                    element.css('min-height', $window.innerHeight + 'px');
                    break;
                }

              }
            }
          })
          .directive('setSeparatorScreenPosition', function($window){
            return{
              link: function(scope,element,attrs){

                element.css('position', 'absolute' );
                switch(element[0].id){
                  case "1":
                    element.css('top', ($window.innerHeight) - 110 + 'px');
                    break;
                  case "2":
                    element.css('top', ($window.innerHeight)*2 +'px' );
                    break;
                  case "3":
                    element.css('top', ($window.innerHeight)*3 +'px' );
                    break;
                }

              }
            }
          })
      .directive('smartChunking', function($window, SmartChunking) {
          return {
              restrict: 'A',
              link: function($scope) {
                  var w = angular.element($window);

                  // window.outerWidth works on desktop, screen.height on iPad (width returns 768)
                  var width = ($window.outerWidth > 0) ? $window.outerWidth : screen.height;
                  var chunkSize = SmartChunking.getChunkSize(width);
                  if (chunkSize !== 5) {
                      $scope.$emit('change-chunk-size', chunkSize);
                  }

                  $scope.getWidth = function() {
                      return ($window.outerWidth > 0) ? $window.outerWidth : screen.width;
                  };

                  $scope.$watch($scope.getWidth, function(newValue, oldValue) {
                      if (newValue !== oldValue) {
                          var chunkSize = SmartChunking.getChunkSize(newValue);
                          $scope.$emit('change-chunk-size', chunkSize);
                      }
                  });

                  w.bind('resize', function() {
                      $scope.$apply();
                  });
              }
          }
      })
      .service('SmartChunking', function() {
          var large = 1600;
          var medium = 760;
          var small = 500;
          var xsmall = 300;

          this.getChunkSize = function(width) {
              var chunkSize;
              if (width >= large) {
                  chunkSize = 5;
              } else if (width >= medium) {
                  chunkSize = 4;
              } else if (width >= small) {
                  chunkSize = 3;
              } else if (width >= xsmall) {
                  chunkSize = 2;
              } else {
                  chunkSize = 1;
              }
              return chunkSize;
          }
      })
          .directive('numbersOnly', function () {
        	    return {
        	        require: 'ngModel',
        	        link: function (scope, element, attr, ngModelCtrl) {
        	            function fromUser(text) {
        	                if (text) {
        	                    var transformedInput = text.replace(/[^0-9]/g, '');

        	                    if (transformedInput !== text) {
        	                        ngModelCtrl.$setViewValue(transformedInput);
        	                        ngModelCtrl.$render();
        	                    }
        	                    return transformedInput;
        	                }
        	                return undefined;
        	            }
        	            ngModelCtrl.$parsers.push(fromUser);
        	        }
        	    };
        	})
          .service('anchorSmoothScroll', function(){

              this.scrollTo = function(eID) {

                  // This scrolling function
                  // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript

                  var startY = currentYPosition();
                  var stopY = elmYPosition(eID);
                  var distance = stopY > startY ? stopY - startY : startY - stopY;
                  if (distance < 100) {
                      scrollTo(0, stopY); return;
                  }
                  var speed = Math.round(distance / 100);
                  if (speed >= 20) speed = 20;
                  var step = Math.round(distance / 25);
                  var leapY = stopY > startY ? startY + step : startY - step;
                  var timer = 0;
                  if (stopY > startY) {
                      for ( var i=startY; i<stopY; i+=step ) {
                          setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                          leapY += step; if (leapY > stopY) leapY = stopY; timer++;
                      } return;
                  }
                  for ( var i=startY; i>stopY; i-=step ) {
                      setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                      leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
                  }

                  function currentYPosition() {
                      // Firefox, Chrome, Opera, Safari
                      if (self.pageYOffset) return self.pageYOffset;
                      // Internet Explorer 6 - standards mode
                      if (document.documentElement && document.documentElement.scrollTop)
                          return document.documentElement.scrollTop;
                      // Internet Explorer 6, 7 and 8
                      if (document.body.scrollTop) return document.body.scrollTop;
                      return 0;
                  }

                  function elmYPosition(eID) {
                      var elm = document.getElementById(eID);
                      var y = elm.offsetTop;
                      var node = elm;
                      while (node.offsetParent && node.offsetParent != document.body) {
                          node = node.offsetParent;
                          y += node.offsetTop;
                      }
                      if(eID=="section1")
                        y=0;
                      return y;
                  }

              };

          })
          .directive('scrollCircular', function($window,$location,anchorSmoothScroll) {
            return function(scope, element, attrs) {
                element.bind("DOMMouseScroll mousewheel onmousewheel", function(event) {

                            // cross-browser wheel delta
                            var event = window.event || event; // old IE support
                            var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
                            event.returnValue = false;
                            // for Chrome and Firefox
                            if(event.preventDefault) {
                              event.preventDefault();
                            }
                            if(delta < 0) {
                              switch(actualSection){
                                case "section1":
                                      //window.scrollTo(0,$window.innerHeight);
                                      actualSection = "section2";
                                      $location.hash(actualSection);
                                      break;
                                case "section2":
                                      actualSection = "section3";
                                      $location.hash(actualSection);
                                      break;
                                case "section3":
                                      actualSection = "section1";
                                      $location.hash(actualSection);
                                      break;
                              }
                              anchorSmoothScroll.scrollTo(actualSection);
                            }
                            else{
                              switch(actualSection){
                                case "section2":
                                      actualSection = "section1";
                                      $location.hash(actualSection);
                                      break;
                                case "section3":
                                      actualSection = "section2";
                                      $location.hash(actualSection);
                                      break;
                              }
                              anchorSmoothScroll.scrollTo(actualSection);
                            }

                });
            };
        });
})();
